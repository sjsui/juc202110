package atguigu_wangze_1.thread_safe_collection_6;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Suisijia
 * @create 2021-10-12 16:22
 */

/*
* 个人版测试：验证Arraylist线程不安全。
*            3个线程对同一个arraylist添加10000个元素，结果略小于30000[如29921]
*
* */

/*
* 补充：实际 应该用CountDownLatch做测试。之后有空可改一下
* */

public class ArrayListUnSafeTest {

    public static void main(String[] args) {

        //List list = new ArrayList<>();

        //解决方式一：
        // List list = new Vector<>();
        //解决方式二：
        //List list = Collections.synchronizedList(new ArrayList<>());
        //解决方式三：
        List list = new CopyOnWriteArrayList<>();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                list.add(UUID.randomUUID().toString());
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                list.add(UUID.randomUUID().toString());
            }
        }, "BB").start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                list.add(UUID.randomUUID().toString());
            }
        }, "CC").start();

        /*
        *
        * 注：此处不能太短，暂定7s。否则上面可能还没执行完
        * */
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }

}

/*
 * 执行结果：30000
 * */
