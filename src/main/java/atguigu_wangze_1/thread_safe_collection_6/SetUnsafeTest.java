package atguigu_wangze_1.thread_safe_collection_6;

import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author Suisijia
 * @create 2021-10-12 20:01
 */

/*
 * 补充：实际 应该用CountDownLatch做测试。之后有空可改一下
 * */

public class SetUnsafeTest {

    public static void main(String[] args) {

        //Set set = new HashSet<>();

        //解决：
        Set set = new CopyOnWriteArraySet<>();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                set.add(UUID.randomUUID().toString().substring(0,8));
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                set.add(UUID.randomUUID().toString().substring(0,8));
            }
        }, "BB").start();

        new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                set.add(UUID.randomUUID().toString().substring(0,8));
            }
        }, "CC").start();

        /*
         *
         * 注：此处不能太短，暂定15s。否则上面可能还没执行完
         * */
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(set.size());
    }
}

/*
* 执行结果：30000
* */