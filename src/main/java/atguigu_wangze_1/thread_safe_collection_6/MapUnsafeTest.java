package atguigu_wangze_1.thread_safe_collection_6;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Suisijia
 * @create 2021-10-12 20:11
 */


/*
 * 补充：实际 应该用CountDownLatch做测试。之后有空可改一下
 * */

public class MapUnsafeTest {
    public static void main(String[] args) {

        //Map map = new HashMap<>();

        //解决：
        Map map = new ConcurrentHashMap();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                map.put(UUID.randomUUID().toString().substring(0,8),UUID.randomUUID().toString().substring(0,8));
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                map.put(UUID.randomUUID().toString().substring(0,8),UUID.randomUUID().toString().substring(0,8));
            }
        }, "BB").start();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                map.put(UUID.randomUUID().toString().substring(0,8),UUID.randomUUID().toString().substring(0,8));
            }
        }, "CC").start();

        /*
         *
         * 注：此处不能太短，暂定5s。否则上面可能还没执行完
         * */
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(map.size());
    }
}

/*
 * 执行结果：3000
 * */