package atguigu_wangze_1.lock_4;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Suisijia
 * @create 2021-10-12 1:40
 */

class ShareResource2 {

    private int num = 0;

    private Lock lock = new ReentrantLock();

    private Condition condition = lock.newCondition();

    public void incr() throws InterruptedException {

        lock.lock();
        try {
            while (num != 0) {
                condition.await();
            }

            num++;
            System.out.println("线程" + Thread.currentThread().getName() + ": " + num);

            condition.signalAll();

        } finally {
            lock.unlock();
        }
    }

    public void decr() throws InterruptedException {
        lock.lock();
        try {
            while (num != 1) {
                condition.await();
            }

            num--;
            System.out.println("线程" + Thread.currentThread().getName() + ": " + num);

            condition.signalAll();

        } finally {
            lock.unlock();
        }
    }

}

public class ThreadNotifyDemo2 {

    public static void main(String[] args) {

        ShareResource2 sr = new ShareResource2();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.incr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "AA").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.decr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "BB").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.incr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "CC").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.decr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "DD").start();

    }

}

/*
* 运行结果示例：
*   线程AA: 1
    线程BB: 0
    线程CC: 1
    线程BB: 0
    线程CC: 1
    线程BB: 0
    线程CC: 1
    线程BB: 0
    线程CC: 1
    线程BB: 0
    线程CC: 1
    线程DD: 0
    线程AA: 1
    线程DD: 0
    线程AA: 1
    线程DD: 0
    线程AA: 1
    线程DD: 0
    线程AA: 1
    线程DD: 0
*
*
* */
