package atguigu_wangze_1.callable_8;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Suisijia
 * @create 2021-10-13 0:32
 */
public class CallableTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask ft1 = new FutureTask(() -> {
            System.out.println("进入ft1对应的call方法");
            return 100;
        });

        FutureTask ft2 = new FutureTask(() -> {
            System.out.println("进入ft2对应的call方法");
            return 200;
        });

        new Thread(ft1, "AA").start();
        new Thread(ft2, "BB").start();

        System.out.println(ft1.get());
        System.out.println(ft2.get());

        // * 个人理解：最后输出这句，说明前面若写了get方法，则一定会等到结果，再向下执行
        System.out.println("main Thread over");

    }

}

/*
* 执行结果示例：
*   进入ft2对应的call方法
    进入ft1对应的call方法
    100
    200
    main Thread over
*
* */
