package atguigu_wangze_1.readwritelock_10;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
* （xialei的代码）
* */
class MyQueue {

	private Object obj;
	
	private ReadWriteLock rwLock = new ReentrantReadWriteLock();
	
	public void readObj() {
		rwLock.readLock().lock();
		try {
			//* 读这个obj对象，因此这样写
			System.out.println(Thread.currentThread().getName()+"\t"+obj);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			rwLock.readLock().unlock();
		}
	}
	
	public void writeObj(Object obj) {
		rwLock.writeLock().lock();
		try {
			//* 写obj对象，因此，obj的值 是从外面传参进来的，是可改变的
			this.obj = obj;
			System.out.println(Thread.currentThread().getName()+"\t"+obj);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			rwLock.writeLock().unlock();
		}
	}
}

/**
 * 
 * @Description: 一个线程写入,100个线程读取
 * @author xialei
 * 
 */
public class ReadWriteLockDemo_xialeiVersion {
	public static void main(String[] args) throws InterruptedException {
		MyQueue myQueue= new MyQueue();
		
		//一写
		new Thread(() -> {
			myQueue.writeObj("JAVA0319!!");
		}, "AA").start();
		
		//为确保 写线程 先执行，可在此处睡0.1s
		//Thread.sleep(100);
			
		//多读
		for (int i = 1; i <= 100; i++) {
			//要将 int类型的i 转换成String类型（** 注：不要写成+""的形式！！太浪费内存空间！！）
			new Thread(() -> {
				myQueue.readObj();
			}, String.valueOf(i)).start();
		}
	}
}

//用于一写多读的 多线程

/*
* 运行结果示例：
* 	AA	JAVA0319!!
	1	JAVA0319!!
	3	JAVA0319!!
	4	JAVA0319!!
	2	JAVA0319!!
	5	JAVA0319!!
	6	JAVA0319!!
	7	JAVA0319!!
	8	JAVA0319!!
	9	JAVA0319!!
	10	JAVA0319!!
	11	JAVA0319!!
	12	JAVA0319!!
	13	JAVA0319!!
	14	JAVA0319!!
	15	JAVA0319!!
	16	JAVA0319!!
	17	JAVA0319!!
	18	JAVA0319!!
	19	JAVA0319!!
	20	JAVA0319!!
	21	JAVA0319!!
	23	JAVA0319!!
	22	JAVA0319!!
	24	JAVA0319!!
	25	JAVA0319!!
	26	JAVA0319!!
	27	JAVA0319!!
	28	JAVA0319!!
	29	JAVA0319!!
	30	JAVA0319!!
	31	JAVA0319!!
	32	JAVA0319!!
	33	JAVA0319!!
	34	JAVA0319!!
	35	JAVA0319!!
	37	JAVA0319!!
	36	JAVA0319!!
	38	JAVA0319!!
	39	JAVA0319!!
	40	JAVA0319!!
	41	JAVA0319!!
	42	JAVA0319!!
	43	JAVA0319!!
	44	JAVA0319!!
	45	JAVA0319!!
	49	JAVA0319!!
	46	JAVA0319!!
	47	JAVA0319!!
	48	JAVA0319!!
	50	JAVA0319!!
	53	JAVA0319!!
	51	JAVA0319!!
	52	JAVA0319!!
	55	JAVA0319!!
	56	JAVA0319!!
	57	JAVA0319!!
	54	JAVA0319!!
	58	JAVA0319!!
	59	JAVA0319!!
	60	JAVA0319!!
	61	JAVA0319!!
	62	JAVA0319!!
	63	JAVA0319!!
	64	JAVA0319!!
	65	JAVA0319!!
	66	JAVA0319!!
	67	JAVA0319!!
	68	JAVA0319!!
	69	JAVA0319!!
	70	JAVA0319!!
	71	JAVA0319!!
	72	JAVA0319!!
	73	JAVA0319!!
	74	JAVA0319!!
	75	JAVA0319!!
	76	JAVA0319!!
	77	JAVA0319!!
	78	JAVA0319!!
	79	JAVA0319!!
	80	JAVA0319!!
	81	JAVA0319!!
	82	JAVA0319!!
	83	JAVA0319!!
	84	JAVA0319!!
	85	JAVA0319!!
	86	JAVA0319!!
	87	JAVA0319!!
	88	JAVA0319!!
	89	JAVA0319!!
	90	JAVA0319!!
	91	JAVA0319!!
	92	JAVA0319!!
	93	JAVA0319!!
	94	JAVA0319!!
	95	JAVA0319!!
	96	JAVA0319!!
	97	JAVA0319!!
	98	JAVA0319!!
	99	JAVA0319!!
	100	JAVA0319!!
*
* */
