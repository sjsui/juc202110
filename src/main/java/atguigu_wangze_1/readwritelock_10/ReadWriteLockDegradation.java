package atguigu_wangze_1.readwritelock_10;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-13 21:39
 */

//演示读写锁降级 （这块有点忘了）
public class ReadWriteLockDegradation {

    public static void main(String[] args) {
        //可重入读写锁对象
        ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
        ReentrantReadWriteLock.ReadLock readLock = rwLock.readLock();//读锁
        ReentrantReadWriteLock.WriteLock writeLock = rwLock.writeLock();//写锁

        //锁降级
        //2 获取读锁
        readLock.lock();
        System.out.println("---read");

        //1 获取写锁
        writeLock.lock();
        System.out.println("atguigu");

        //3 释放写锁
        //writeLock.unlock();

        //4 释放读锁
        //readLock.unlock();
    }

}
