package atguigu_wangze_1.synchronized_1;

/**
 * @author Suisijia
 * @create 2021-10-11 20:18
 */

// * 小点：最终 会调用 native 的 start0方法，
//        在操作系统层面 创建线程

class Ticket {

    private int num = 30;

    /* 经个人测试：若把synchronized注释掉，则明显可见3个一组的肉眼效果，并且最后出现超卖 */
    public synchronized void sale() {

        if (num > 0) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "卖出第" + num-- + "张票，还剩" + num + "张");
        }

    }
}

public class SynchronizedSaleTicket {

    public static void main(String[] args) {

        Ticket ticket = new Ticket();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 40; i++) {
                    ticket.sale();
                }
            }
        }, "AA").start();

        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.sale();
            }
        }, "BB").start();

        new Thread(() -> {
            for (int i = 0; i < 40; i++) {
                ticket.sale();
            }
        }, "CC").start();

    }

}


/*
运行结果示例：
        AA卖出第30张票，还剩29张
        AA卖出第29张票，还剩28张
        AA卖出第28张票，还剩27张
        AA卖出第27张票，还剩26张
        AA卖出第26张票，还剩25张
        AA卖出第25张票，还剩24张
        AA卖出第24张票，还剩23张
        AA卖出第23张票，还剩22张
        AA卖出第22张票，还剩21张
        AA卖出第21张票，还剩20张
        AA卖出第20张票，还剩19张
        AA卖出第19张票，还剩18张
        AA卖出第18张票，还剩17张
        AA卖出第17张票，还剩16张
        AA卖出第16张票，还剩15张
        AA卖出第15张票，还剩14张
        AA卖出第14张票，还剩13张
        AA卖出第13张票，还剩12张
        AA卖出第12张票，还剩11张
        AA卖出第11张票，还剩10张
        AA卖出第10张票，还剩9张
        AA卖出第9张票，还剩8张
        AA卖出第8张票，还剩7张
        CC卖出第7张票，还剩6张
        CC卖出第6张票，还剩5张
        CC卖出第5张票，还剩4张
        CC卖出第4张票，还剩3张
        CC卖出第3张票，还剩2张
        CC卖出第2张票，还剩1张
        CC卖出第1张票，还剩0张
*/
