package atguigu_wangze_1.lock_5;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Suisijia
 * @create 2021-10-12 1:51
 */

class ShareResource5 {

    private int flag = 1;

    private Lock lock = new ReentrantLock();

    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();


    public void print5(int lunshu) throws InterruptedException {

        lock.lock();

        try {
            while (flag != 1) {
                c1.await();
            }

            for (int i = 0; i < 5; i++) {
                System.out.println("线程" + Thread.currentThread().getName() + ": " + lunshu);
            }

            flag = 2;
            c2.signal();
        } finally {
            lock.unlock();
        }

    }

    public void print10(int lunshu) throws InterruptedException {

        lock.lock();

        try {
            while (flag != 2) {
                c2.await();
            }

            for (int i = 0; i < 10; i++) {
                System.out.println("线程" + Thread.currentThread().getName() + ": " + lunshu);
            }

            flag = 3;
            c3.signal();
        } finally {
            lock.unlock();
        }
    }

    public void print15(int lunshu) throws InterruptedException {

        lock.lock();

        try {
            while (flag != 3) {
                c3.await();
            }

            for (int i = 0; i < 15; i++) {
                System.out.println("线程" + Thread.currentThread().getName() + ": " + lunshu);
            }

            flag = 1;
            c1.signal();
        } finally {
            lock.unlock();
        }
    }

}



public class CustomizedThreadNotifyDemo {

    public static void main(String[] args) {

        ShareResource5 sr5 = new ShareResource5();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    sr5.print5(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"AA").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    sr5.print10(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"BB").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    sr5.print15(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"CC").start();

    }

}
