package atguigu_wangze_1.utilclass_9;

import java.util.concurrent.CyclicBarrier;

//集齐7颗龙珠就可召唤神龙
public class CyclicBarrierDemo {

    //创建固定值
    private static final int NUMBER = 7;

    public static void main(String[] args) {
        //创建CyclicBarrier
        CyclicBarrier cyclicBarrier =
                new CyclicBarrier(NUMBER,()->{
                    System.out.println("*****集齐7颗龙珠就可召唤神龙");
                });

        //集齐七颗龙珠过程
        for (int i = 1; i <= 7; i++) {
            new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName()+" 星龙被收集到了");
                    //等待
                    cyclicBarrier.await();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },String.valueOf(i)).start();
        }
    }
}

/*
 * 是 在多个线程内，写 cyclicBarrier.await();
 *
 * 个人理解：
 * 多个线程 人到齐后，一起去执行一个事情。主角是这多个线程
 *
 * （个人理解：且 可反复执行 此过程，有点 找齐多个线程进度 的感觉）
 * */
