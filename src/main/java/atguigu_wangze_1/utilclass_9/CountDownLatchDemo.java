package atguigu_wangze_1.utilclass_9;

import java.util.concurrent.CountDownLatch;

//演示 CountDownLatch
public class CountDownLatchDemo {
    //6个同学陆续离开教室之后，班长锁门
    public static void main(String[] args) throws InterruptedException {

        //创建CountDownLatch对象，设置初始值
        CountDownLatch countDownLatch = new CountDownLatch(6);

        //6个同学陆续离开教室之后
        for (int i = 1; i <=6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+" 号同学离开了教室");

                //计数  -1
                countDownLatch.countDown();

            },String.valueOf(i)).start();
        }

        //等待
        countDownLatch.await();

        System.out.println(Thread.currentThread().getName()+" 班长锁门走人了");
    }
}

/*
* 运行结果示例：
*   1 号同学离开了教室
    4 号同学离开了教室
    3 号同学离开了教室
    6 号同学离开了教室
    2 号同学离开了教室
    5 号同学离开了教室
    main 班长锁门走人了
*
* */

/*
* 是 在多个线程内，写 countDownLatch.countDown();
*    在主线程，写 countDownLatch.await();
*
* 个人理解：
* 一个线程 等待 多个线程完成后，执行它的事情。主角是这个主线程
* */