package atguigu_wangze_1.utilclass_9;

/**
 * @author Suisijia
 * @create 2021-10-13 19:34
 */

//测试可知，班长会提前锁门

public class NoUseCountDownLatch {
    public static void main(String[] args) throws InterruptedException {

        //6个同学陆续离开教室之后
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+" 号同学离开了教室");

            },String.valueOf(i)).start();
        }

        System.out.println(Thread.currentThread().getName()+" 班长锁门走人了");
    }
}

/*
* 运行结果示例：
*   1 号同学离开了教室
    4 号同学离开了教室
    3 号同学离开了教室
    main 班长锁门走人了
    2 号同学离开了教室
    6 号同学离开了教室
    5 号同学离开了教室
*
* */
