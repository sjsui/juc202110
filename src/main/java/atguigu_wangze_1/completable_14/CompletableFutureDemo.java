package atguigu_wangze_1.completable_14;

import java.util.concurrent.CompletableFuture;

//异步调用 和 同步调用
public class CompletableFutureDemo {
    public static void main(String[] args) throws Exception {
        //异步调用 无返回值
        CompletableFuture<Void> completableFuture1 = CompletableFuture.runAsync(()->{
            System.out.println(Thread.currentThread().getName()+" : CompletableFuture1");
        });
        completableFuture1.get();

        //异步调用 有返回值
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(()->{
            System.out.println(Thread.currentThread().getName()+" : CompletableFuture2");
            //模拟异常
            int i = 10/0;
            return 1024;
        });
        completableFuture2.whenComplete((t,u)->{
            System.out.println("------t="+t);   //返回值
            System.out.println("------u="+u);   //异常
        }).get();

    }
}
