package atguigu_wangze_1.synchronized_3;

/**
 * @author Suisijia
 * @create 2021-10-12 1:26
 */

class ShareResource1 {

    private int num = 0;

    public synchronized void incr() throws InterruptedException {
        while (num != 0) {
            wait();
        }

        num++;
        System.out.println("线程" + Thread.currentThread().getName() + ": " + num);

        notifyAll();
    }

    public synchronized void decr() throws InterruptedException {
        while (num != 1) {
            wait();
        }

        num--;
        System.out.println("线程" + Thread.currentThread().getName() + ": " + num);

        notifyAll();
    }

}


public class ThreadNotifyDemo1 {

    public static void main(String[] args) {

        ShareResource1 sr = new ShareResource1();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.incr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } ,"AA").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.decr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } ,"BB").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.incr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } ,"CC").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    sr.decr();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } ,"DD").start();

    }
}

/*
* 运行结果示例：
*   线程AA: 1
    线程BB: 0
    线程AA: 1
    线程BB: 0
    线程AA: 1
    线程BB: 0
    线程AA: 1
    线程BB: 0
    线程CC: 1
    线程BB: 0
    线程AA: 1
    线程DD: 0
    线程CC: 1
    线程DD: 0
    线程CC: 1
    线程DD: 0
    线程CC: 1
    线程DD: 0
    线程CC: 1
    线程DD: 0
*
* */