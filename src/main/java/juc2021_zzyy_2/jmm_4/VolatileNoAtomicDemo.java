package juc2021_zzyy_2.jmm_4;

import java.util.concurrent.TimeUnit;

/**
 * @auther zzyy
 * @create 2021-03-17 14:59
 */

/*
* 打印结果 小于 10000。如 main	9917
*
* **说明了 volatile不保证原子性
*
* */
class MyNumber {

    //int number = 0;             //方式一：若用 普通变量，不保证原子性
    volatile int number = 0;      //方式二：若用 volatile变量，也不保证原子性 (经多次测试，好像是比方式一略好)

    public void addPlusPlus()
    {
        number++;
    }
}

public class VolatileNoAtomicDemo {
    public static void main(String[] args) throws InterruptedException {
        MyNumber myNumber = new MyNumber();

        //10个线程，每个线程 循环加1000次
        for (int i = 1; i <=10; i++) {
            new Thread(() -> {
                for (int j = 1; j <= 1000; j++) {
                    myNumber.addPlusPlus();
                }
            },String.valueOf(i)).start();
        }

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
        System.out.println(Thread.currentThread().getName() + "\t" + myNumber.number);  //main	9666
    }
}

