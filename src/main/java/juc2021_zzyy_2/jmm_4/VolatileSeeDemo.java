package juc2021_zzyy_2.jmm_4;

import java.util.concurrent.TimeUnit;

/**
 * @auther zzyy
 * @create 2021-03-15 19:13
 */

//【经测试，结果都是可以停止】
// **个人猜测理解：
//      volatile保证的只是 线程A写完后，立即被线程B可见。
//                          [个人猜测理解：貌似这样脏读的概率会小，但还是有可能脏读。即不保证原子性]
//      而此类的示例中，只是有看见早或晚的区别，实际都能读到修改后的变量
public class VolatileSeeDemo {
    //static boolean flag = true;         //不加volatile，没有可见性。因此程序不会停止【不对啊，测试结果好像都是可以停止啊？？个人猜测：可能与static的变量有关，没验证】
    volatile static boolean flag = true;  //加了volatile，保证可见性。t1可执行完毕【不对啊，测试结果好像都是可以停止啊？？个人猜测：可能与static的变量有关，没验证】

    public static void main(String[] args) {
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName()+"\t"+"---come in");
            while(flag) {
                new Integer(308);
                System.out.println("--------------");
            }
            System.out.println("t1 over");
        },"t1").start();

        //确保t2在t1后面执行
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            flag = false;
        },"t2").start();
    }
}
