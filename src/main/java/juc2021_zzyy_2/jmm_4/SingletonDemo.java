package juc2021_zzyy_2.jmm_4;

/**
 * @auther zzyy
 * @create 2021-03-19 19:21
 */

// 静态内部类方式 的 单例模式
public class SingletonDemo {
    private SingletonDemo() { }

    private static class SingletonDemoHandler {
        private static SingletonDemo instance = new SingletonDemo();
    }

    public static SingletonDemo getInstance()
    {
        return SingletonDemoHandler.instance;
    }
}
