package juc2021_zzyy_2.rwlock_11;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * @auther zzyy
 * @create 2021-03-28 11:04
 */

/*
* 阳哥原版代码
*
* */

class MyResource {

    Map<String, String> map = new HashMap<>();
    //=====ReentrantLock 等价于 =====synchronized
    Lock lock = new ReentrantLock();
    //=====ReentrantReadWriteLock 一体两面，读写互斥，读读共享
    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();


    public void write(String key, String value) {
        rwLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在写入");
            map.put(key, value);

            try { TimeUnit.MILLISECONDS.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成写入");
        } finally {
            rwLock.writeLock().unlock();
        }
    }

    public void read(String key) {
        rwLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在读取");
            String result = map.get(key);

            //暂停几秒钟线程
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成读取result： " + result);
        } finally {
            rwLock.readLock().unlock();
        }
    }
}


public class ReentrantReadWriteLockDemo {
    public static void main(String[] args) {
        MyResource myResource = new MyResource();

        for (int i = 1; i <= 5; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource.write(finalI + "", finalI + "");
            }, String.valueOf(i)).start();
        }

        for (int i = 1; i <= 5; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource.read(finalI + "");
            }, String.valueOf(i)).start();
        }

        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource.write(finalI + "", finalI + "");
            }, "MYC" + String.valueOf(i)).start();
        }

        /*
        *（个人理解：说明了  只有读读是共享的）
        * 某1次打印结果：
        *   1	---正在写入
            1	---完成写入
            2	---正在写入
            2	---完成写入
            5	---正在写入
            5	---完成写入
            1	---正在读取
            4	---正在读取
            5	---正在读取
            1	---完成读取result： 1
            5	---完成读取result： 5
            4	---完成读取result： null
            3	---正在写入
            3	---完成写入
            4	---正在写入
            4	---完成写入
            2	---正在读取
            3	---正在读取
            2	---完成读取result： 2
            3	---完成读取result： 3
            MYC1	---正在写入
            MYC1	---完成写入
            MYC2	---正在写入
            MYC2	---完成写入
            MYC3	---正在写入
            MYC3	---完成写入
        *
        *
        * 某2次打印结果：
        *   1	---正在写入
            1	---完成写入
            2	---正在写入
            2	---完成写入
            3	---正在写入
            3	---完成写入
            4	---正在写入
            4	---完成写入
            5	---正在写入
            5	---完成写入
            1	---正在读取
            4	---正在读取
            4	---完成读取result： 4
            1	---完成读取result： 1
            MYC3	---正在写入
            MYC3	---完成写入
            3	---正在读取
            2	---正在读取
            2	---完成读取result： 2
            3	---完成读取result： 3
            MYC1	---正在写入
            MYC1	---完成写入
            MYC2	---正在写入
            MYC2	---完成写入
            5	---正在读取
            5	---完成读取result： 5
        *
        * */

    }
}

