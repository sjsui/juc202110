package juc2021_zzyy_2.rwlock_11.private_exer;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-26 3:50
 */

/*
* 写锁 可降级为 读锁 示例
* */
public class ReadWriteLockDownGradingDemo1 {

    public static void main(String[] args) {

        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();

        writeLock.lock();
        System.out.println("-----write lock");

        readLock.lock();
        System.out.println("-----read lock");


        writeLock.unlock();
        System.out.println("-----write unlock");

        readLock.unlock();
        System.out.println("-----read unlock");

        /*
        * 打印结果：
        *   -----write lock
            -----read lock
            -----write unlock
            -----read unlock
        *
        *
        * */
    }

}
