package juc2021_zzyy_2.rwlock_11.private_exer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-26 3:02
 */

/*
* 个人测试代码：
*
*  ** 个人理解：此示例 说明了 正在读的线程 全部完成，才能有 新写进入
*
* */

public class ReentrantReadWriteLockDemo2 {

    public static void main(String[] args) {
        MyResource2 myResource2 = new MyResource2();

        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource2.write(finalI + "", finalI + "");
            }, String.valueOf(i)).start();
        }

        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource2.read(finalI + "");
            }, String.valueOf(i)).start();
        }


        // ** 总是在最后执行。个人理解：说明了 正在读的线程 全部完成，才能有 新写进入
        // 【个人理解：因此尤其容易造成锁饥饿】
        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                myResource2.write(finalI + "", finalI + "");
            }, "another batch write thread" + String.valueOf(i)).start();
        }


        /*
        * 某次打印结果：
            1	---正在写入
            1	---完成写入
            2	---正在写入
            2	---完成写入
            3	---正在写入
            3	---完成写入
            1	---正在读取
            2	---正在读取
            3	---正在读取
            3	---完成读取result： 3
            2	---完成读取result： 2
            1	---完成读取result： 1
            another batch write thread1	---正在写入
            another batch write thread1	---完成写入
            another batch write thread2	---正在写入
            another batch write thread2	---完成写入
            another batch write thread3	---正在写入
            another batch write thread3	---完成写入
        *
        *
        * */

    }
}


class MyResource2 {
    Map<String, String> map = new HashMap<>();
    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

    public void read(String key) {
        rwLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在读取");

            String result = map.get(key);
            //暂停一下线程，分开上下两个打印语句。
            //为了更明显说明 读锁是共享锁，可 多个读 同时进出
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成读取result： " + result);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    public void write(String key, String value) {
        rwLock.writeLock().lock();

        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在写入");

            map.put(key, value);
            //暂停一下线程，分开上下两个打印语句。
            //为了更明显说明 写锁是独占锁，一个一个进出
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成写入");
        } finally {
            rwLock.writeLock().unlock();
        }

    }
}









