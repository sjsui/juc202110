package juc2021_zzyy_2.rwlock_11.private_exer;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-26 3:50
 */

/*
* 读锁 不可升级为 写锁 示例
* */
public class ReadWriteLockDownGradingDemo2 {

    public static void main(String[] args) {

        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();

        readLock.lock();
        System.out.println("-----read lock");

        writeLock.lock();
        System.out.println("-----write lock");

        readLock.unlock();
        System.out.println("-----read unlock");

        writeLock.unlock();
        System.out.println("-----write unlock");


        /*
        * 打印结果：（即 卡住了）
        * -----read lock
        *
        * */
    }

}
