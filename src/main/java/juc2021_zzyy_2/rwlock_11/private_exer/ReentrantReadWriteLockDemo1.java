package juc2021_zzyy_2.rwlock_11.private_exer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-26 3:02
 */

/*
* 个人测试代码：
* 此示例 说明了 写锁是独占锁，读锁是共享锁
*
* */

public class ReentrantReadWriteLockDemo1 {

    public static void main(String[] args) {
        MyResource1 myResource1 = new MyResource1();

        //开启几个 写线程（保持写读线程名的一致，方便put数据）
        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                //（注：若在此停一下，则会先执行读【说明了此处 先打印写 后打印读 只是上下顺序的关系而已】）
                //try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }
                //变成String再put
                myResource1.write(finalI + "", finalI + "");
            }, String.valueOf(i)).start();
        }

        //开启几个 读线程（保持写读线程名的一致，方便put数据）
        for (int i = 1; i <= 3; i++) {
            int finalI = i;
            new Thread(() -> {
                //变成String
                myResource1.read(finalI + "");
            }, String.valueOf(i)).start();
        }



        /*
        * 某次打印结果：
            2	---正在写入
            2	---完成写入
            3	---正在写入
            3	---完成写入
            1	---正在写入
            1	---完成写入
            2	---正在读取
            3	---正在读取
            1	---正在读取
            3	---完成读取result： 3
            2	---完成读取result： 2
            1	---完成读取result： 1
        *
        *
        * */

    }
}



class MyResource1 {
    Map<String, String> map = new HashMap<>();
    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();

    public void read(String key) {
        rwLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在读取");

            String result = map.get(key);
            //暂停一下线程，分开上下两个打印语句。
            //为了更明显说明 读锁是共享锁，可 多个读 同时进出
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成读取result： " + result);
        } finally {
            rwLock.readLock().unlock();
        }
    }

    public void write(String key, String value) {
        rwLock.writeLock().lock();

        try {
            System.out.println(Thread.currentThread().getName() + "\t" + "---正在写入");

            map.put(key, value);
            //暂停一下线程，分开上下两个打印语句。
            //为了更明显说明 写锁是独占锁，一个一个进出
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "\t" + "---完成写入");
        } finally {
            rwLock.writeLock().unlock();
        }

    }
}









