package juc2021_zzyy_2.rwlock_11.private_exer;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Suisijia
 * @create 2021-10-26 3:50
 */

/*
* 个人理解：锁降级 的 典型应用 示例
* 【个人理解：这样可确保 其他的读 优先。 写不进来】
* */
public class ReadWriteLockDownGradingDemo3 {

    public static void main(String[] args) {

        ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

        ReentrantReadWriteLock.ReadLock readLock = readWriteLock.readLock();
        ReentrantReadWriteLock.WriteLock writeLock = readWriteLock.writeLock();



        writeLock.lock();
        System.out.println("-----write lock");
        try {
            //业务代码start
            //...

            //想本次写完 立刻被读取。

            //...
            //业务代码end
            readLock.lock();
            System.out.println("-----read lock");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writeLock.unlock();
            System.out.println("-----write unlock");
        }

        //个人理解：后面还需再手动释放读锁
/*      try {
            // ...
        } finally {
           readLock.unlock();
        }
*/


        /*
        * 打印结果：
            -----write lock
            -----read lock
            -----write unlock
        *
        * */
    }

}
