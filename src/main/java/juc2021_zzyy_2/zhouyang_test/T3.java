package juc2021_zzyy_2.zhouyang_test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Suisijia
 * @create 2021-10-19 19:15
 */

/*
* 个人理解：一次map[貌似略快] 和 两次map 的效率比较
* */
public class T3 {

    private static List<Integer> list1 = new ArrayList<>();
    private static List<Integer> list2 = new ArrayList<>();

    //【两次map】costTime: 500毫秒左右
//    public static void main(String[] args) {
//        IntStream.range(0, 3000000).forEach(list1::add);
//
//        long startTime = System.currentTimeMillis();
//        List<Integer> collect1 = list1.stream().map(m -> m * 2).map(m -> m * 2).collect(Collectors.toList());
//        long endTime = System.currentTimeMillis();
//        System.out.println("costTime: " + (endTime - startTime) + "毫秒");
//    }


    //【一次map】costTime: 380毫秒左右
    public static void main(String[] args) {
        IntStream.range(0, 3000000).forEach(list2::add);

        long startTime2 = System.currentTimeMillis();
        List<Integer> collect2 = list2.stream().map(m -> m * 2).collect(Collectors.toList());
        long endTime2 = System.currentTimeMillis();
        System.out.println("costTime: " + (endTime2 - startTime2) + "毫秒");
    }

}
