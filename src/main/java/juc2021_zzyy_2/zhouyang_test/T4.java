package juc2021_zzyy_2.zhouyang_test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @author Suisijia
 * @create 2021-10-19 19:16
 */

//  interrupt() 也可唤醒 正在park() 的线程（但这么写不规范。了解即可）
public class T4 {

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {
            System.out.println("t1 ---park之前 中断标志位" + "\t" + Thread.currentThread().isInterrupted());
            LockSupport.park();
            System.out.println("t1 ---park之后 中断标志位" + "\t" + Thread.currentThread().isInterrupted());
            System.out.println(Thread.currentThread().getName() + "\t" + "---被唤醒" + Thread.currentThread().isInterrupted());
            System.out.println();
        }, "t1");

        t1.start();

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();


        //非活动状态后，查一下t1的中断标志位。
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("**************" + t1.isInterrupted());


        /*
        * 打印结果：
        *   t1 ---park之前 中断标志位	false
            t1 ---park之后 中断标志位	true
            t1	---被唤醒true

            **************false
        *
        * */
    }

}
