package juc2021_zzyy_2.zhouyang_test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

/**
 * @author Suisijia
 * @create 2021-10-19 19:15
 */

/*
* 个人理解：阳哥推荐用串行流。并行流 虽然略快，但可能有线程安全问题
* */
public class T2 {
    private static List<Integer> list1 = new ArrayList<>();
    private static List<Integer> list2 = new ArrayList<>();
    private static List<Integer> list3 = new ArrayList<>();
    private static Lock lock = new ReentrantLock();

    public static void main(String[] args) {

        IntStream.range(0, 10000).forEach(list1::add);  //串行流
        IntStream.range(0, 10000).parallel().forEach((value) -> list2.add(value)); //并行流
        IntStream.range(0, 10000).forEach(i -> {
            lock.lock();
            try {
                list3.add(i);
            } finally {
                lock.unlock();
            }
        });

        System.out.println("串行执行的大小：" + list1.size());
        System.out.println("并行执行的大小：" + list2.size());
        System.out.println("加锁并行执行的大小：" + list3.size());

        /*
        *   执行结果：
        *   串行执行的大小：10000
            并行执行的大小：7434  【虽然略快，但有线程安全问题】
            加锁并行执行的大小：10000
        *
        * */

    }
}
