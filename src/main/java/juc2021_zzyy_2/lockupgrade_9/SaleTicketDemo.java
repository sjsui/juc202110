package juc2021_zzyy_2.lockupgrade_9;

class TrainTicket {
    private int number = 50;

    Object objectLock = new Object();

    public void sale() {
        synchronized (objectLock) {
            if(number>0) {
                System.out.println(Thread.currentThread().getName()+"\t"+"---卖出第： "+(number--));
            }
        }
    }
}


/**
 * @auther zzyy
 * @create 2021-03-27 11:27
 */
public class SaleTicketDemo {
    public static void main(String[] args) {
        TrainTicket trainTicket = new TrainTicket();

        new Thread(() -> { for (int i = 1; i <=55 ; i++) trainTicket.sale(); },"t1").start();
        new Thread(() -> { for (int i = 1; i <=55 ; i++) trainTicket.sale(); },"t2").start();
        new Thread(() -> { for (int i = 1; i <=55 ; i++) trainTicket.sale(); },"t3").start();
        new Thread(() -> { for (int i = 1; i <=55 ; i++) trainTicket.sale(); },"t4").start();

    }
}

/*
* 结果示例：
*   t1	---卖出第： 50
    t1	---卖出第： 49
    t1	---卖出第： 48
    t1	---卖出第： 47
    t1	---卖出第： 46
    t1	---卖出第： 45
    t1	---卖出第： 44
    t2	---卖出第： 43
    t2	---卖出第： 42
    t2	---卖出第： 41
    t2	---卖出第： 40
    t2	---卖出第： 39
    t2	---卖出第： 38
    t2	---卖出第： 37
    t2	---卖出第： 36
    t2	---卖出第： 35
    t2	---卖出第： 34
    t2	---卖出第： 33
    t2	---卖出第： 32
    t2	---卖出第： 31
    t2	---卖出第： 30
    t2	---卖出第： 29
    t2	---卖出第： 28
    t2	---卖出第： 27
    t2	---卖出第： 26
    t2	---卖出第： 25
    t2	---卖出第： 24
    t2	---卖出第： 23
    t2	---卖出第： 22
    t2	---卖出第： 21
    t2	---卖出第： 20
    t2	---卖出第： 19
    t2	---卖出第： 18
    t2	---卖出第： 17
    t2	---卖出第： 16
    t2	---卖出第： 15
    t2	---卖出第： 14
    t2	---卖出第： 13
    t2	---卖出第： 12
    t2	---卖出第： 11
    t2	---卖出第： 10
    t2	---卖出第： 9
    t2	---卖出第： 8
    t2	---卖出第： 7
    t2	---卖出第： 6
    t2	---卖出第： 5
    t2	---卖出第： 4
    t2	---卖出第： 3
    t2	---卖出第： 2
    t2	---卖出第： 1
*
*
* */