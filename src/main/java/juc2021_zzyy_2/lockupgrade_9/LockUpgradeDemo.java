package juc2021_zzyy_2.lockupgrade_9;

import org.openjdk.jol.info.ClassLayout;

/**
 * @auther zzyy
 * @create 2021-03-27 10:59
 */
public class LockUpgradeDemo {
    public static void main(String[] args) {
        //-XX:+UseBiasedLocking[默认已开] -XX:BiasedLockingStartupDelay=0
        //【个人测试：一个线程时：
        //              启动时 若不加参数，打印结果为 轻量级锁
        //                    加上面这行的参数，打印结果为 偏向锁
        //          多个线程时：
        //              都是重量级锁】
        Object o = new Object();


        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                synchronized (o) {
                    System.out.println(ClassLayout.parseInstance(o).toPrintable());
                }
            }).start();
        }
    }

    public static void noLock() {
        Object o = new Object();
        System.out.println(o.hashCode());
        System.out.println(Integer.toHexString(o.hashCode()));

        System.out.println(ClassLayout.parseInstance(o).toPrintable());
    }
}

/*
执行结果：
    多个线程时：
    【可见 多个线程 竞争的情况下，都升级为了重量级锁[锁标志位为 10]】
        java.lang.Object object internals:
        OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
        0     4        (object header)                           3a 22 c2 1c (00111010 00100010 11000010 00011100) (482484794)
        4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
        8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
        12     4        (loss due to the next object alignment)
        Instance size: 16 bytes
        Space losses: 0 bytes internal + 4 bytes external = 4 bytes total

        java.lang.Object object internals:
        OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
        0     4        (object header)                           3a 22 c2 1c (00111010 00100010 11000010 00011100) (482484794)
        4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
        8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
        12     4        (loss due to the next object alignment)
        Instance size: 16 bytes
        Space losses: 0 bytes internal + 4 bytes external = 4 bytes total

        java.lang.Object object internals:
        OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
        0     4        (object header)                           3a 22 c2 1c (00111010 00100010 11000010 00011100) (482484794)
        4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
        8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
        12     4        (loss due to the next object alignment)
        Instance size: 16 bytes
        Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
*/

