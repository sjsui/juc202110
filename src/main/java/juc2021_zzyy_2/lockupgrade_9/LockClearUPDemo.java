package juc2021_zzyy_2.lockupgrade_9;

/**
 * @auther zzyy
 * @create 2021-03-27 15:17
 * 锁消除
 * 从JIT角度看 相当于无视它，
 * synchronized (o)不存在了,这个锁对象 并没有被共用 扩散到其它线程使用，
 *
 * 极端地说 就是 根本没有 加 这个锁对象 的 底层机器码，消除了锁的使用
 */
public class LockClearUPDemo {
    static Object objectLock = new Object();//正常的,有且仅有同一把锁

    public void m1() {
        //锁消除。JIT会无视它，synchronized(对象锁)不存在了。不正常的
        //【个人理解：即相当于每个线程进入方法时，都创建了一个对象、作为一把锁。没锁住】
        Object objectLock = new Object();

        synchronized (objectLock) {
            System.out.println("----hello lock");
        }
    }

    public static void main(String[] args) {

    }
}
