package juc2021_zzyy_2.lockupgrade_9;

import org.openjdk.jol.info.ClassLayout;

/**
 * @auther zzyy
 * @create 2021-03-28 17:02
 */
public class MyObject {
    public static void main(String[] args) {
        noLock();
        biasedLock();
    }

    public static void biasedLock() {
        Object o = new Object();

        new Thread(() -> {
            synchronized (o) {
                System.out.println( ClassLayout.parseInstance(o).toPrintable());
            }
        },"t1").start();
    }


    public static void noLock() {
        Object o = new Object();
        System.out.println("十进制hash码:" + o.hashCode());//10进制
        System.out.println("十六进制hash码:" + Integer.toHexString(o.hashCode()));//16进制
        System.out.println("二进制hash码:" + Integer.toBinaryString(o.hashCode()));//2进制

        //这段是阳哥的说明：（自己的打印结果和他不一样）
        //00100011111111000110001001011110
        //  100011111111000110001001011110

        System.out.println();

        System.out.println(ClassLayout.parseInstance(o).toPrintable());

        /*
        * 打印结果：
        *   十进制hash码:1836019240
            十六进制hash码:6d6f6e28
            二进制hash码:1101101011011110110111000101000

            【无锁的情况】
            java.lang.Object object internals:
             OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
                  0     4        (object header)                           01 28 6e 6f (00000001 00101000 01101110 01101111) (1869490177)
                  4     4        (object header)                           6d 00 00 00 (01101101 00000000 00000000 00000000) (109)
                  8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
                 12     4        (loss due to the next object alignment)
            Instance size: 16 bytes
            Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
        *
        *
        * 【偏向锁的情况。[因为锁标志位为 101] 个人理解：因为只有一个线程 用这个锁】
        * 【为啥此时不用开 -XX:BiasedLockingStartupDelay=0 的参数就有效果了？？
        *   个人猜测可能是与主线程执行的东西有关？？】
        * java.lang.Object object internals:
             OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
                  0     4        (object header)                           05 48 8b 1f (00000101 01001000 10001011 00011111) (529221637)
                  4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
                  8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
                 12     4        (loss due to the next object alignment)
            Instance size: 16 bytes
            Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
        * */
    }

}
