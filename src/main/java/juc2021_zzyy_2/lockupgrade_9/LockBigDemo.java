package juc2021_zzyy_2.lockupgrade_9;

import java.util.HashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @auther zzyy
 * @create 2021-03-27 15:19
 * 锁粗化
 * 假如 方法中 首尾相接，前后相邻的 都是同一个锁对象，
 * 那JIT编译器 就会把这几个synchronized块 合并成一个大块，
 * 加粗加大范围，一次申请锁使用 即可，避免次次的申请和释放锁，提升了性能
 */
public class LockBigDemo {
    static Object objectLock = new Object();

    public static void main(String[] args) {
        //原版写法：
        new Thread(() -> {
            synchronized (objectLock) {
                System.out.println("11111");
            }
            synchronized (objectLock) {
                System.out.println("22222");
            }
            synchronized (objectLock) {
                System.out.println("33333");
            }
        },"a").start();


        //个人理解：优化后的效果：
/*        new Thread(() -> {
            synchronized (objectLock) {
                System.out.println("-------1");

                System.out.println("-------2");


                System.out.println("-------3");


                System.out.println("-------4");
            }
        },"t1").start();
*/


        Lock lock = new ReentrantLock();

        lock.lock();
        System.out.println("---------11111");
        lock.unlock();

        new HashMap<>().put(1,1);

    }
}
