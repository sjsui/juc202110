package juc2021_zzyy_2.atomics_6;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicReference;


@ToString
@AllArgsConstructor
@Getter
class User {
    String userName;
    int    age;
}


/**
 * @auther zzyy
 * @create 2021-03-19 11:39
 */

//（此类不用看了，前面已演示过了）

public class AtomicReferenceDemo {
    public static void main(String[] args) {
        User z3 = new User("z3",24);
        User li4 = new User("li4",26);

        AtomicReference<User> atomicReferenceUser = new AtomicReference<>();

        atomicReferenceUser.set(z3);

        System.out.println(atomicReferenceUser.compareAndSet(z3,li4)+"\t"+atomicReferenceUser.get().toString());
        System.out.println(atomicReferenceUser.compareAndSet(z3,li4)+"\t"+atomicReferenceUser.get().toString());

    }


    /*
    * 运行结果：
    *   true	User(userName=li4, age=26)
        false	User(userName=li4, age=26)
    *
    * */
}
