package juc2021_zzyy_2.atomics_6;

import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @auther zzyy
 * @create 2021-03-18 16:42
 */
public class AtomicIntegerArrayDemo {
    public static void main(String[] args) {
        //构造方式1
        AtomicIntegerArray atomicIntegerArray1 = new AtomicIntegerArray(5);
        //构造方式2
        AtomicIntegerArray atomicIntegerArray2 = new AtomicIntegerArray(new int[5]);
        //构造方式3
        AtomicIntegerArray atomicIntegerArray3 = new AtomicIntegerArray(new int[]{1,2,3,4,5});

        for (int i = 0; i < atomicIntegerArray1.length(); i++) {
            System.out.print(atomicIntegerArray1.get(i) + " "); //0 0 0 0 0
        }

        System.out.println();

        int tmpInt = 0;

        tmpInt = atomicIntegerArray1.getAndSet(0,1122);
        System.out.println(tmpInt+"\t" + atomicIntegerArray1.get(0)); //0	1122

        atomicIntegerArray1.getAndIncrement(1);
        atomicIntegerArray1.getAndIncrement(1);
        tmpInt = atomicIntegerArray1.getAndIncrement(1);
        System.out.println(tmpInt+"\t" + atomicIntegerArray1.get(1)); //2	3


        for (int i = 0; i < atomicIntegerArray1.length(); i++) {
            System.out.print(atomicIntegerArray1.get(i) + " "); //1122 3 0 0 0
        }

    }
}
