package juc2021_zzyy_2.atomics_6;

import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;

/**
 * @auther zzyy
 * @create 2021-03-19 15:59
 */

/*
*  LongAdder 和 LongAccumulator 的demo
*
*  LongAdder：只能做加法，且只能从0开始（了解）
*  LongAccumulator：都可，更灵活
* */
public class LongAdderAPIDemo {
    public static void main(String[] args) {
        LongAdder longAdder = new LongAdder();//只能做加法

        longAdder.increment();
        longAdder.increment();
        longAdder.increment();

        System.out.println(longAdder.longValue());     //3



/*      匿名类对象 的 写法
        LongAccumulator longAccumulator = new LongAccumulator(new LongBinaryOperator() {
            @Override
            public long applyAsLong(long left, long right) {
                return left - right;
            }
        }, 100);    */


        LongAccumulator longAccumulator = new LongAccumulator((x,y) -> x-y , 100);

        longAccumulator.accumulate(1);//1
        longAccumulator.accumulate(2);//3
        longAccumulator.accumulate(3);//6

        System.out.println(longAccumulator.longValue());    //94


    }
}
