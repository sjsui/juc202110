package juc2021_zzyy_2.atomics_6;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

class MyNumber {
    AtomicInteger atomicInteger = new AtomicInteger();

    public void addPlusPlus() {
        atomicInteger.incrementAndGet();
    }
}

/**
 * @auther zzyy
 * @create 2021-03-17 16:26
 */


/*
*  ** AtomicInteger 和 CountDownLatch的应用
* */
public class AtomicIntegerDemo {
    public static final int SIZE = 50;

    public static void main(String[] args) throws InterruptedException {

        MyNumber myNumber = new MyNumber();
        CountDownLatch countDownLatch = new CountDownLatch(SIZE);

        for (int i = 1; i <= SIZE; i++) {
            new Thread(() -> {
                try {
                    for (int j = 1; j <= 1000; j++) {
                        myNumber.addPlusPlus();
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
                //确保每个线程干完活后，要countDown()
                finally {
                    countDownLatch.countDown();
                }
            },String.valueOf(i)).start();
        }

        //解决方式一：（傻办法）
        //try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        //解决方式二：【确保一满足条件后，就立即执行】
        countDownLatch.await();



        System.out.println(Thread.currentThread().getName()+"\t"+"---result : "+myNumber.atomicInteger.get());

        /*
        * 最初打印结果：main	---result : 34809
        * 执行后 打印结果：main	---result : 50000
        *
        * */

    }
}

