package juc2021_zzyy_2.atomics_6;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicMarkableReference;

/**
 * @auther zzyy
 * @create 2021-03-22 14:14
 */

/*
* AtomicMarkableReference的应用
* */
public class AtomicMarkableReferenceDemo {

    static AtomicMarkableReference atomicMarkableReference = new AtomicMarkableReference(100,false);

    public static void main(String[] args) {
        //线程t3偷梁换柱。但操作 记录了 是否修改过 的标记
        new Thread(() -> {
            boolean marked = atomicMarkableReference.isMarked();
            System.out.println(Thread.currentThread().getName()+"\t"+"---默认修改标识："+marked);

            //（了解）（此处等1秒，只是为了让 t4 获得 和 t3一样的 默认修改标志，都是false，好比较）
            try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

            // * 线程t3偷梁换柱
            //(注意此处 参数3和参数4 的写法，应直接使用原默认值mark,而不是用atomicMarkableReference.isMarked()。否则就正好逻辑错了。阳哥应该是写错了）
            atomicMarkableReference.compareAndSet(100,101,marked,!marked);
            System.out.println(Thread.currentThread().getName()+"\t 修改1次后的版本号" + atomicMarkableReference.isMarked());

            //(注意此处 参数3和参数4 的写法，应直接使用原默认值mark,而不是用atomicMarkableReference.isMarked()。否则就正好逻辑错了。阳哥应该是写错了）
            atomicMarkableReference.compareAndSet(101,100,marked,!marked);
            System.out.println(Thread.currentThread().getName()+"\t 修改2次后的版本号" + atomicMarkableReference.isMarked());
        },"t3").start();


        try { TimeUnit.MILLISECONDS.sleep(10); } catch (InterruptedException e) { e.printStackTrace(); }


        new Thread(() -> {
            boolean marked = atomicMarkableReference.isMarked();
            System.out.println(Thread.currentThread().getName()+"\t"+"---默认修改标识："+marked);

            //等待 t3先偷梁换柱
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

            // * t4成功发现 期望值100 之前已被人动过。因此操作失败
            boolean b = atomicMarkableReference.compareAndSet(100, 20210308, marked, !marked);
            System.out.println(Thread.currentThread().getName()+"\t"+"---操作是否成功:"+b);


            System.out.println(Thread.currentThread().getName()+"\t"+atomicMarkableReference.getReference());//t4	101。表示 当前值为 101
            System.out.println(Thread.currentThread().getName()+"\t"+atomicMarkableReference.isMarked()); //t4	true。 表示 期望值100 已被动过

        },"t4").start();

        /*
        * 打印结果：
        *   t3	---默认修改标识：false
            t4	---默认修改标识：false
            t3	 修改1次后的版本号true
            t3	 修改2次后的版本号true
            t4	---操作是否成功:false
            t4	101
            t4	true
        *
        * */
    }
}
