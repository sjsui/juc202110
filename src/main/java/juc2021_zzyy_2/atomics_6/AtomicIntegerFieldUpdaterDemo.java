package juc2021_zzyy_2.atomics_6;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

class BankAccount {
    String bankName = "ccb";

    //以 一种线程安全的方式 操作 非线程安全对象 内 的某些字段

    //1 更新的对象属性 必须使用 public volatile 修饰符。
    // (否则执行时会报 java.lang.IllegalArgumentException: Must be volatile type)
    public volatile int money = 0;

    //2 因为 对象的属性修改原子类 都是抽象类，所以每次使用
    // 都必须 使用静态方法 newUpdater()创建一个更新器，并需设置 想要更新的 类和属性。
    AtomicIntegerFieldUpdater fieldUpdater = AtomicIntegerFieldUpdater.newUpdater(BankAccount.class,"money");

    public void transfer(BankAccount bankAccount) {
        fieldUpdater.incrementAndGet(bankAccount);
    }

/*
    方式二：【结合阳哥说法的个解：
                    这种方式也可做到，但 是另一个体系 的 写法。适用于不同的应用场景。且这种方式 内存占用 更大。
                    现在的前提是 给一个线程不安全的对象 的 部分属性 加锁。
                    因此应 用方式一】
    private AtomicInteger money = new AtomicInteger(0);

    public AtomicInteger getMoney() {
        return money;
    }

    public void transfer(BankAccount bankAccount) {
        money.incrementAndGet();
    }
*/

}


/**
 * @auther zzyy
 * @create 2021-03-18 17:20
 */
public class AtomicIntegerFieldUpdaterDemo {
    public static void main(String[] args) throws InterruptedException {
        BankAccount bankAccount = new BankAccount();

        for (int i = 1; i <= 1000; i++) {
            new Thread(() -> {
                bankAccount.transfer(bankAccount);
            },String.valueOf(i)).start();
        }

        //暂停几秒钟线程（此处最好改用CountDownLatch）
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        //在主线程中 显示结果
        //方式一：
        System.out.println(Thread.currentThread().getName()+"\t"+"---bankAccount: "+bankAccount.money);
        //方式二：
        //System.out.println(Thread.currentThread().getName()+"\t"+"---bankAccount: "+bankAccount.getMoney());

        /*
        * 方式一 打印结果：main	---bankAccount: 1000
        * 方式二 打印结果：main	---bankAccount: 1000
        * */
    }
}
