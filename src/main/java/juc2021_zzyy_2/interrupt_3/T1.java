package juc2021_zzyy_2.interrupt_3;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


class MyNumber {
    boolean flag = true;
}
/**
 * @auther zzyy
 * @create 2021-03-03 18:25
 */
public class T1 {
    static volatile boolean isStop = false;
    static AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    public static void main(String[] args) {
        //m1();
        //m2();
        m3();
        //m4();
    }

    // 用一个volatile修饰的变量，优雅地停止线程
    public static void m1() {
        new Thread(() -> {
            while(true) {
                if(isStop) {
                    System.out.println(Thread.currentThread().getName()+"线程------isStop = true,自己退出了");
                    break;
                }
                try { TimeUnit.MILLISECONDS.sleep(300); } catch (InterruptedException e) { e.printStackTrace(); }
                System.out.println("-------hello interrupt");
            }
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
        isStop = true;
    }
    /*
    * 执行结果：
    *   -------hello interrupt
        -------hello interrupt
        -------hello interrupt
        -------hello interrupt
        t1线程------isStop = true,自己退出了
    *
    * */


    // 用一个AtomicBoolean类型 修饰的变量，优雅地停止线程
    public static void m2() {
        new Thread(() -> {
            while(true) {
                if(atomicBoolean.get()) {
                    System.out.println(Thread.currentThread().getName()+"线程------atomicBoolean.get() = true,自己退出了");
                    break;
                }
                try { TimeUnit.MILLISECONDS.sleep(300); } catch (InterruptedException e) { e.printStackTrace(); }
                System.out.println("-------hello interrupt2");
            }
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
        atomicBoolean.set(true);
    }
    /*
    * 执行结果：
    *   -------hello interrupt2
        -------hello interrupt2
        -------hello interrupt2
        -------hello interrupt2
        t1线程------atomicBoolean.get() = true,自己退出了
    *
    *
    * */


    // **(有个小坑) 调用 线程的interrupt()，优雅地停止线程
    public static void m3() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println(Thread.currentThread().getName() + "线程------isInterrupted() = true,自己退出了");
                    break;
                }

                // ** 注：与前两种不同，不能在此处用sleep()，否则会导致无法停止线程。
                //          即老师讲的那个 对正在sleep的线程执行interrupt，会抛异常并清除中断标志位【个人已测试验证】
                //      （若就是想加sleep，则可在catch中，再interrupt申请中断一下，将中断标志位为true）【个人已测试验证】
                try { TimeUnit.MILLISECONDS.sleep(300); } catch (InterruptedException e) {
                    //Thread.currentThread().interrupt();   //【** 个解：要加上此句，否则会无限循环】
                    e.printStackTrace();
                }

                System.out.println("-------hello interrupt3");
            }
        }, "t1");
        t1.start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }
        t1.interrupt();
    }


    //（这种方式暂没细看。个解：就是用一个类的属性 做变量，也可使得 线程中断）
    public static void m4() {
        MyNumber myNumber = new MyNumber();

        new Thread(() -> {
            System.out.println("----come in");
            while(myNumber.flag) {
                new Integer(5);
            }
            System.out.println("-----t1 process is over");
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            myNumber.flag = false;
        },"t2").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println(Thread.currentThread().getName()+"\t"+myNumber.flag);
    }

}