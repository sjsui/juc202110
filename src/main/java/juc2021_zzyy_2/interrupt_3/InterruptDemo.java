package juc2021_zzyy_2.interrupt_3;

import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @auther zzyy
 * @create 2021-03-03 18:20
 */
public class InterruptDemo {

    // volatile修饰的变量 与 可见性 有关
    static volatile boolean isStop = false;
    AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    //（test7同test5的示例。在main()中才能演示出效果，因此复制过来了一份）
    public static void main(String[] args) {
        test7();
    }


    public static void test7() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("-----isInterrupted() = true，程序结束。");
                    break;
                }
                try {
                    // * 1）不加这句可正常结束。2）加了这句后，不能正确停止【经测试：在main()中才有此效果。】
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // * 3）加了这句后，又变回能正确停止了
                    // ** 出现InterruptedException时，线程的中断标志位 会复位为false,无法停下。需再次手动调interrupt()设置true
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }

                //？？？当不设置睡眠时，不知道为什么第一行控制台打印了个这个：errupt（这个问题暂时忽略，与主题不相关。猜测可能与junit有关）
                System.out.println("----hello Interrupt");
            }
        }, "t1");
        t1.start();

        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            t1.interrupt();//修改t1线程的中断标志位为true
        },"t2").start();
    }


    //对Thread.interrupted()的演示：
    @Test
    public void test6() {
        System.out.println(Thread.currentThread().getName()+"---"+Thread.interrupted());//查看当前线程 中断标志，并置为false
        System.out.println(Thread.currentThread().getName()+"---"+Thread.interrupted());//查看当前线程 中断标志，并置为false
        System.out.println("111111");
        Thread.currentThread().interrupt();///----false---> true 将当前线程的中断标志位 置为false
        System.out.println("222222");//正常打印，说明并没立即停止
        System.out.println(Thread.currentThread().getName()+"---"+Thread.interrupted());//查看当前线程 中断标志，并置为false
        System.out.println(Thread.currentThread().getName()+"---"+Thread.interrupted());//查看当前线程 中断标志，并置为false

        /*
        * 打印结果：
        *   main---false
            main---false
            111111
            222222
            main---true
            main---false
        *
        * */
    }


    // ** 重要示例。
    // （注：但在junit中不出效果，出了异常就直接停了，也没正常退出。
    //      因此，个人猜测，junit中，只要遇到异常就结束）
    @Test
    public void test5() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("-----isInterrupted() = true，程序结束。");
                    break;
                }
                try {
                    // * 1）不加这句可正常结束。2）加了这句后，不能正确停止
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    // * 3）加了这句后，又变回能正确停止了
                    Thread.currentThread().interrupt();
                }

                //？？？当不设置睡眠时，不知道为什么第一行控制台打印了个这个：errupt（这个问题暂时忽略，与主题不相关。猜测可能与junit有关）
                System.out.println("----hello Interrupt");
            }
        }, "t1");
        t1.start();

        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            t1.interrupt();//修改t1线程的中断标志位为true
        },"t2").start();
    }

    /**
     * 此示例说明了：中断为true后，并不是立刻stop程序
     */
    @Test
    public void test4() {
        //中断为true后，并不是立刻stop程序
        Thread t1 = new Thread(() -> {
            for (int i = 1; i <= 200; i++) {
                System.out.println("------i: " + i);
            }
            //3)
            System.out.println("【此句在t1线程中打印，此时t1线程已执行完200次循环】t1.interrupt()调用之后02： "+Thread.currentThread().isInterrupted());
        }, "t1");
        t1.start();

        //1)
        System.out.println("【此句在主线程中打印】t1.interrupt()调用之前,t1线程的中断标识默认值： "+t1.isInterrupted());

        //3秒之后，申请打断t1线程【注：此时t1线程已执行了3毫秒左右】
        //实例方法interrupt() 仅仅是将 线程的中断状态位 设置为true，不会立即停止线程
        try { TimeUnit.MILLISECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
        t1.interrupt();

        // 2)活动状态,t1线程还在执行中
        System.out.println("【此句在主线程中打印】t1.interrupt()调用之后01： "+t1.isInterrupted());

        try { TimeUnit.MILLISECONDS.sleep(3000); } catch (InterruptedException e) { e.printStackTrace(); }

        // 4)非活动状态,t1线程已结束执行。因此为false
        //  （了解：[已多次测试]在JDK17中测试时，此句打印为true）
        System.out.println("【此句在主线程中打印】t1.interrupt()调用之后03： "+t1.isInterrupted());
    }


    //【不用看此处了，看T1类那个即可】
    @Test
    public void test3() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("-----isInterrupted() = true，程序结束。");
                    break;
                }
                //？？？当不设置睡眠时，不知道为什么第一行控制台打印了个这个：errupt （这个问题暂时忽略，与主题不相关。猜测可能与junit有关）
                System.out.println("----hello Interrupt");
            }
        }, "t1");
        t1.start();

        try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            t1.interrupt();//修改t1线程 的 中断标志位 为true
        },"t2").start();
    }

    /**
     * 通过AtomicBoolean【不用看此处了，看T1类那个即可】
     */
    @Test
    public void test2() {
        new Thread(() -> {
            while(true) {
                if(atomicBoolean.get()) {
                    System.out.println("-----atomicBoolean.get() = true，程序结束。");
                    break;
                }
                System.out.println("------hello atomicBoolean");
            }
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            atomicBoolean.set(true);
        },"t2").start();
    }

    /**
     * 通过一个volatile变量实现 【不用看此处了，看T1类那个即可】
     */
    @Test
    public void test1() {
        new Thread(() -> {
            while(true) {
                if(isStop) {
                    System.out.println("-----isStop = true，程序结束。");
                    break;
                }
                System.out.println("------hello isStop");
            }
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            isStop = true;
        },"t2").start();
    }
}
