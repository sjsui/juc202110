package juc2021_zzyy_2.interrupt_3.lock_support_demo;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Suisijia
 * @create 2021-10-19 0:15
 */

//道理上 和SyncWaitNotifyDemo 一模一样
public class LockAwaitSignalDemo {

    static Lock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();

    //使用 Lock的 await()和signal()的缺点：阻塞。
    // * 1 因为await()和signal() 必须写在lock中，不能单独用；
    //   2 其次，必须先执行await()，再执行signal()才有效果。否则无法唤醒
    public static void main(String[] args) {
        new Thread(() -> {
            //若在此处暂停几秒钟线程，则会发现无法唤醒
            //try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName()+"\t"+"---come in");
                condition.await();
                System.out.println(Thread.currentThread().getName()+"\t"+"---被唤醒");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"t1").start();

        //暂停几秒钟线程(目的是 确保 先执行t1 再执行t2)
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            lock.lock();
            try {
                condition.signal();
                System.out.println(Thread.currentThread().getName()+"\t"+"---发出通知");
            } finally {
                lock.unlock();
            }
        },"t2").start();
    }

}
