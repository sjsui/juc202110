package juc2021_zzyy_2.interrupt_3.lock_support_demo;

import java.util.concurrent.TimeUnit;

/**
 * @author Suisijia
 * @create 2021-10-18 19:26
 */
public class SyncWaitNotifyDemo {
    static Object objectLock = new Object();
    //使用synchronized 的 wait()和notify()的缺点：阻塞。
    // * 1 因为wait()和notify() 必须写在synchronized中，不能单独用；
    //   2 其次，必须先执行wait()，再执行notify()才有效果。否则无法唤醒
    //【注：本例若放到junit中测试时，睡眠时的情况，有问题】
    public static void main(String[] args) {
        new Thread(() -> {
            //若在此处暂停几秒钟线程，则会发现无法唤醒
            //try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            synchronized (objectLock){
                System.out.println(Thread.currentThread().getName()+"\t"+"---come in t1 thread");
                try {
                    objectLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"\t"+"---t1 thread 被唤醒");
            }
        },"t1").start();

        //暂停几秒钟线程(目的是 确保 先执行t1 再执行t2)
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            synchronized (objectLock){
                objectLock.notify();
                System.out.println(Thread.currentThread().getName()+"\t"+"---t2 thread 发出通知");
            }
        },"t2").start();
    }
}
