package juc2021_zzyy_2.locks_2;

/**
 * @auther zzyy
 * @create 2021-03-03 15:21
 * 从字节码角度 分析synchronized实现
 * （暂没自己试）
 */
public class LockByteCodeDemo {
    final Object object = new Object();

    public void m1() {
        synchronized (object) {
            System.out.println("----------hello sync");
            throw new RuntimeException("----ex");
        }
    }

    /*public synchronized void m2() {

    }*/

    public static synchronized void m2() {

    }
}
