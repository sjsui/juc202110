package juc2021_zzyy_2.locks_2;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Ticket {
    private int number = 50;

    private Lock lock = new ReentrantLock(false); //默认用的是非公平锁，分配的平均一点，=--》公平一点
    public void sale() {
        lock.lock();
        try {
            if(number > 0) {
                System.out.println(Thread.currentThread().getName()+"\t 卖出第: "+(number--)+"\t 还剩下: "+number);
            }
        } finally {
            lock.unlock();
        }
    }



    /*Object objectLock = new Object();

    public void sale()
    {
        synchronized (objectLock)
        {
            if(number > 0)
            {
                System.out.println(Thread.currentThread().getName()+"\t 卖出第: "+(number--)+"\t 还剩下: "+number);
            }
        }
    }*/
}

/**
 * @auther zzyy
 * @create 2020-07-09 17:48
 */
public class SaleTicketDemo {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();

        new Thread(() -> { for (int i = 1; i <=55; i++) ticket.sale(); },"a").start();
        new Thread(() -> { for (int i = 1; i <=55; i++) ticket.sale(); },"b").start();
        new Thread(() -> { for (int i = 1; i <=55; i++) ticket.sale(); },"c").start();
        new Thread(() -> { for (int i = 1; i <=55; i++) ticket.sale(); },"d").start();
        new Thread(() -> { for (int i = 1; i <=55; i++) ticket.sale(); },"e").start();
    }
}


/*
* 非公平锁 结果示例：
*   a	 卖出第: 50	 还剩下: 49
    a	 卖出第: 49	 还剩下: 48
    a	 卖出第: 48	 还剩下: 47
    a	 卖出第: 47	 还剩下: 46
    a	 卖出第: 46	 还剩下: 45
    a	 卖出第: 45	 还剩下: 44
    a	 卖出第: 44	 还剩下: 43
    a	 卖出第: 43	 还剩下: 42
    a	 卖出第: 42	 还剩下: 41
    a	 卖出第: 41	 还剩下: 40
    a	 卖出第: 40	 还剩下: 39
    a	 卖出第: 39	 还剩下: 38
    a	 卖出第: 38	 还剩下: 37
    a	 卖出第: 37	 还剩下: 36
    a	 卖出第: 36	 还剩下: 35
    a	 卖出第: 35	 还剩下: 34
    a	 卖出第: 34	 还剩下: 33
    a	 卖出第: 33	 还剩下: 32
    a	 卖出第: 32	 还剩下: 31
    a	 卖出第: 31	 还剩下: 30
    a	 卖出第: 30	 还剩下: 29
    a	 卖出第: 29	 还剩下: 28
    a	 卖出第: 28	 还剩下: 27
    a	 卖出第: 27	 还剩下: 26
    a	 卖出第: 26	 还剩下: 25
    a	 卖出第: 25	 还剩下: 24
    a	 卖出第: 24	 还剩下: 23
    a	 卖出第: 23	 还剩下: 22
    a	 卖出第: 22	 还剩下: 21
    a	 卖出第: 21	 还剩下: 20
    a	 卖出第: 20	 还剩下: 19
    a	 卖出第: 19	 还剩下: 18
    a	 卖出第: 18	 还剩下: 17
    a	 卖出第: 17	 还剩下: 16
    d	 卖出第: 16	 还剩下: 15
    d	 卖出第: 15	 还剩下: 14
    d	 卖出第: 14	 还剩下: 13
    d	 卖出第: 13	 还剩下: 12
    d	 卖出第: 12	 还剩下: 11
    d	 卖出第: 11	 还剩下: 10
    d	 卖出第: 10	 还剩下: 9
    d	 卖出第: 9	 还剩下: 8
    d	 卖出第: 8	 还剩下: 7
    d	 卖出第: 7	 还剩下: 6
    d	 卖出第: 6	 还剩下: 5
    d	 卖出第: 5	 还剩下: 4
    d	 卖出第: 4	 还剩下: 3
    d	 卖出第: 3	 还剩下: 2
    d	 卖出第: 2	 还剩下: 1
    d	 卖出第: 1	 还剩下: 0
*
*
* */

/*
* 公平锁 结果示例：
*   a	 卖出第: 50	 还剩下: 49
    a	 卖出第: 49	 还剩下: 48
    a	 卖出第: 48	 还剩下: 47
    a	 卖出第: 47	 还剩下: 46
    a	 卖出第: 46	 还剩下: 45
    a	 卖出第: 45	 还剩下: 44
    a	 卖出第: 44	 还剩下: 43
    b	 卖出第: 43	 还剩下: 42
    a	 卖出第: 42	 还剩下: 41
    c	 卖出第: 41	 还剩下: 40
    b	 卖出第: 40	 还剩下: 39
    a	 卖出第: 39	 还剩下: 38
    c	 卖出第: 38	 还剩下: 37
    b	 卖出第: 37	 还剩下: 36
    d	 卖出第: 36	 还剩下: 35
    a	 卖出第: 35	 还剩下: 34
    c	 卖出第: 34	 还剩下: 33
    b	 卖出第: 33	 还剩下: 32
    d	 卖出第: 32	 还剩下: 31
    a	 卖出第: 31	 还剩下: 30
    c	 卖出第: 30	 还剩下: 29
    e	 卖出第: 29	 还剩下: 28
    b	 卖出第: 28	 还剩下: 27
    d	 卖出第: 27	 还剩下: 26
    a	 卖出第: 26	 还剩下: 25
    c	 卖出第: 25	 还剩下: 24
    e	 卖出第: 24	 还剩下: 23
    b	 卖出第: 23	 还剩下: 22
    d	 卖出第: 22	 还剩下: 21
    a	 卖出第: 21	 还剩下: 20
    c	 卖出第: 20	 还剩下: 19
    e	 卖出第: 19	 还剩下: 18
    b	 卖出第: 18	 还剩下: 17
    d	 卖出第: 17	 还剩下: 16
    a	 卖出第: 16	 还剩下: 15
    c	 卖出第: 15	 还剩下: 14
    e	 卖出第: 14	 还剩下: 13
    b	 卖出第: 13	 还剩下: 12
    d	 卖出第: 12	 还剩下: 11
    a	 卖出第: 11	 还剩下: 10
    c	 卖出第: 10	 还剩下: 9
    e	 卖出第: 9	 还剩下: 8
    b	 卖出第: 8	 还剩下: 7
    d	 卖出第: 7	 还剩下: 6
    a	 卖出第: 6	 还剩下: 5
    c	 卖出第: 5	 还剩下: 4
    e	 卖出第: 4	 还剩下: 3
    b	 卖出第: 3	 还剩下: 2
    d	 卖出第: 2	 还剩下: 1
    a	 卖出第: 1	 还剩下: 0
*
*
* */
