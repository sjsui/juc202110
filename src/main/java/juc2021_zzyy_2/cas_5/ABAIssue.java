package juc2021_zzyy_2.cas_5;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Suisijia
 * @create 2021-10-21 16:27
 */
public class ABAIssue {

    static AtomicInteger atomicInteger = new AtomicInteger(100);

    public static void main(String[] args) {
        //线程t1偷梁换柱
        new Thread(() -> {
            atomicInteger.compareAndSet(100,101);
            atomicInteger.compareAndSet(101,100);
        },"t1").start();

        //暂停毫秒
        try { TimeUnit.MILLISECONDS.sleep(10); } catch (InterruptedException e) { e.printStackTrace(); }

        //线程t2仍然正常执行
        new Thread(() -> {
            boolean b = atomicInteger.compareAndSet(100, 20210308);
            System.out.println(Thread.currentThread().getName()+"\t"+"修改成功否："+b+"\t"+atomicInteger.get());
        },"t2").start();

        /*
        * 打印结果：
        * t2	修改成功否：true	20210308
        *
        * */
    }
}
