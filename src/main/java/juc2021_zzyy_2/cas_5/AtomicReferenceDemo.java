package juc2021_zzyy_2.cas_5;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @auther zzyy
 * @create 2021-03-19 22:13
 */

// ** 个人理解：即 可把一个普通类，变为原子类
public class AtomicReferenceDemo {
    public static void main(String[] args) {
        User z3 = new User("z3",24);
        User li4 = new User("li4",26);

        AtomicReference<User> atomicReferenceUser = new AtomicReference<>();

        atomicReferenceUser.set(z3);

        System.out.println(atomicReferenceUser.compareAndSet(z3,li4)+"\t"+atomicReferenceUser.get().toString());
        System.out.println(atomicReferenceUser.compareAndSet(z3,li4)+"\t"+atomicReferenceUser.get().toString());

        /*
        * 打印结果：
        *   true	User(userName=li4, age=26)
            false	User(userName=li4, age=26)
        *
        * */

    }
}
@Getter
@ToString
@AllArgsConstructor
class User {
    String userName;
    int    age;
}