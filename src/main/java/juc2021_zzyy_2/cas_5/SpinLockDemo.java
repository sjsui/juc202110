package juc2021_zzyy_2.cas_5;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @auther zzyy
 * @create 2021-03-17 15:21
 * 题目：实现一个自旋锁
 * 自旋锁好处：循环 比较获取。
 *            没有类似wait的阻塞。
 *
 * 通过CAS操作 完成自旋锁，A线程先进来 调用myLock方法 自己持有锁3秒钟，
 *                       B随后进来 发现当前有线程持有锁，不是null，所以只能通过自旋等待，直到A释放锁后 B随后抢到。
 */
public class SpinLockDemo {
    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    public void MyLock() {
        System.out.println(Thread.currentThread().getName()+"\t"+"---come in");
        //自旋抢锁
        while(!atomicReference.compareAndSet(null,Thread.currentThread())) {
            //（若把下面这行放开，则几秒内，会打印好多排---------waiting-----------）
            //System.out.println("---------------waiting-----------------");
        }
        System.out.println(Thread.currentThread().getName()+"\t"+"---持有锁成功");
    }

    public void MyUnLock() {
        atomicReference.compareAndSet(Thread.currentThread(),null);
        System.out.println(Thread.currentThread().getName()+"\t"+"---释放锁成功");
    }


    //测试：资源类 操作 线程
    public static void main(String[] args) {
        SpinLockDemo spinLockDemo = new SpinLockDemo();

        new Thread(() -> {
            spinLockDemo.MyLock();
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            spinLockDemo.MyUnLock();
        },"t1").start();

        new Thread(() -> {
            spinLockDemo.MyLock();
            spinLockDemo.MyUnLock();
        },"t2").start();

        /*
        * 打印结果：
        *   t1	---come in
            t1	---持有锁成功
            t2	---come in

            3秒之后
            t1	---释放锁成功
            t2	---持有锁成功
            t2	---释放锁成功
        *
        * */

    }

}
