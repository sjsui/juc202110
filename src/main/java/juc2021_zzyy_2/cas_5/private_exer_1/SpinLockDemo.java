package juc2021_zzyy_2.cas_5.private_exer_1;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;


public class SpinLockDemo {

    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    public void myLock() {
        System.out.println(Thread.currentThread().getName() + "---------come in");
        while (!atomicReference.compareAndSet(null, Thread.currentThread())) {

        }
        System.out.println(Thread.currentThread().getName() + "----------拿到锁");
    }

    public void myUnLock() {
        atomicReference.compareAndSet(Thread.currentThread(),null);
        System.out.println(Thread.currentThread().getName() + "----------释放锁");
    }


    public static void main(String[] args) throws Exception {

        SpinLockDemo spinLockDemo = new SpinLockDemo();


        new Thread(() -> {
            spinLockDemo.myLock();
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
            spinLockDemo.myUnLock();
        },"t1").start();

        TimeUnit.MILLISECONDS.sleep(100);

        new Thread(() -> {
            spinLockDemo.myLock();
            spinLockDemo.myUnLock();
        },"t2").start();
    }













//    AtomicReference<Thread> atomicReference = new AtomicReference<>();
//
//    public void MyLock() {
//        System.out.println(Thread.currentThread().getName()+"\t"+"---come in");
//        //自旋抢锁
//        while(!atomicReference.compareAndSet(null,Thread.currentThread())) {
//            System.out.println("---------------waiting-----------------");
//        }
//        System.out.println(Thread.currentThread().getName()+"\t"+"---持有锁成功");
//    }
//
//    public void MyUnLock() {
//        atomicReference.compareAndSet(Thread.currentThread(),null);
//        System.out.println(Thread.currentThread().getName()+"\t"+"---释放锁成功");
//    }


    //测试：资源类 线程 操作
//    public static void main(String[] args) {
//        SpinLockDemo spinLockDemo = new SpinLockDemo();
//
//        new Thread(() -> {
//            spinLockDemo.MyLock();
//            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }
//            spinLockDemo.MyUnLock();
//        },"t1").start();
//
//        new Thread(() -> {
//            spinLockDemo.MyLock();
//            spinLockDemo.MyUnLock();
//        },"t2").start();
//    }
}
