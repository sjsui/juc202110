package juc2021_zzyy_2.cas_5;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @auther zzyy
 * @create 2021-03-17 15:13
 */
public class CASDemo {
    public static void main(String[] args) {

        AtomicInteger atomicInteger = new AtomicInteger(5);
        System.out.println(atomicInteger.get());


        System.out.println(atomicInteger.compareAndSet(5, 308)+"\t"+atomicInteger.get());

        System.out.println(atomicInteger.compareAndSet(5, 3333)+"\t"+atomicInteger.get());
    }

    /*
    * 打印结果：
    *   5
        true	308
        false	308
    *
    * */
}
