package juc2021_zzyy_2.cas_5;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @author Suisijia
 * @create 2021-10-21 16:34
 */

/*
* AtomicStampedReference的应用
* 【** 个解：即 可把一个普通类，变为原子类，并解决ABA问题】
* */
public class ABAResolve {
    static AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(100,1);

    public static void main(String[] args) {
        //线程t3偷梁换柱。但操作 记录了 版本号
        new Thread(() -> {
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t"+"---默认版本号: "+stamp);

            //（了解）（此处等1秒，只是为了让 t4 获得 和 t3一样的 版本号，都是1，好比较）
            try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

            // * 线程t3偷梁换柱
            atomicStampedReference.compareAndSet(100,101,stamp,stamp+1);
            System.out.println(Thread.currentThread().getName()+"\t"+"---1次版本号: "+atomicStampedReference.getStamp());

            atomicStampedReference.compareAndSet(101,100,atomicStampedReference.getStamp(),atomicStampedReference.getStamp() + 1);
            System.out.println(Thread.currentThread().getName()+"\t"+"---2次版本号: "+atomicStampedReference.getStamp());
        },"t3").start();


        try { TimeUnit.MILLISECONDS.sleep(10); } catch (InterruptedException e) { e.printStackTrace(); }


        new Thread(() -> {
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t"+"---默认版本号: "+stamp);

            //等待 t3先偷梁换柱
            try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }

            // * t4成功发现 期望值100 之前已被人动过。因此操作失败
            boolean result = atomicStampedReference.compareAndSet(100, 20210308, stamp, stamp + 1);
            System.out.println(Thread.currentThread().getName()+"\t"+"---操作成功否："+result+"\t"+atomicStampedReference.getStamp()+"\t"+atomicStampedReference.getReference());
        },"t4").start();

        /*
        * 打印结果：
        *   t3	---默认版本号: 1
            t4	---默认版本号: 1
            t3	---1次版本号: 2
            t3	---2次版本号: 3
            t4	---操作成功否：false	3	100
        *
        *
        * */
    }
}
