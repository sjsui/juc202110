package juc2021_zzyy_2.completableFuture_1;


import java.util.concurrent.*;

/**
 * @auther zzyy
 * @create 2021-03-02 11:56
 */

//(了解。硬要还用futureTask时期的那种 的get() 的 老套路写法，也可以写。但不好)
//【即 CompletableFuture的 不太好的用法】
public class CompletableFutureDemo {
    public static void main(String[] args)throws Exception {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

        CompletableFuture<Void> cf1 = CompletableFuture.runAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "-----come in");
        });
        System.out.println(cf1.get());

        CompletableFuture<Void> cf2 = CompletableFuture.runAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "-----come in");
        }, threadPoolExecutor);
        System.out.println(cf2.get());

        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "-----come in");
            return 100;
        });
        System.out.println(cf3.get());

        CompletableFuture<Integer> cf4 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "-----come in");
            return 200;
        },threadPoolExecutor);
        System.out.println(cf4.get());


        threadPoolExecutor.shutdown();
    }
}

/*
* 执行结果：
* （说明了，是一个一个地 有阻塞地 依次执行）
*   ForkJoinPool.commonPool-worker-1	-----come in
    null
    pool-1-thread-1	-----come in
    null
    ForkJoinPool.commonPool-worker-1	-----come in
    100
    pool-1-thread-1	-----come in
    200
*
*
* */