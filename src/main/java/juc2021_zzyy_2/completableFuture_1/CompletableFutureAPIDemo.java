package juc2021_zzyy_2.completableFuture_1;

import org.junit.Test;

import java.util.concurrent.*;

/**
 * @auther zzyy
 * @create 2021-03-02 17:54
 */
public class CompletableFutureAPIDemo {

    @Test
    public void test6() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

        System.out.println(CompletableFuture.supplyAsync(() -> {
            return 1;
        }).thenApply(f -> {
            return f + 2;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("0-------result: " + v);
            }
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        }).join());

        threadPoolExecutor.shutdown();

        /*
        * 执行结果：
        *       0-------result: 3
                3
        * */
    }

    /**
     * thenCombine
     */
    @Test
    public void test5() {
/*
        写法一：
        System.out.println(CompletableFuture.supplyAsync(() -> {
            return 10;
        }).thenCombine(CompletableFuture.supplyAsync(() -> {
            return 20;
        }), (r1, r2) -> {
            return r1 + r2;
        }).join());

        // 执行结果：30
*/

        //写法二：
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "---come in ");
            return 10;
        });
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "---come in ");
            return 20;
        });

        CompletableFuture<Integer> thenCombineResult = cf1.thenCombine(cf2, (x, y) -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "---come in ");
            return x + y;
        });

        System.out.println(thenCombineResult.join());


        /*
        * 执行结果【以下情况都有可能（注：此为在JDK17下运行[已多次测试]）】：
        *
            ForkJoinPool.commonPool-worker-1	---come in
            ForkJoinPool.commonPool-worker-2	---come in
            ForkJoinPool.commonPool-worker-2	---come in
            30

            ForkJoinPool.commonPool-worker-2	---come in
            ForkJoinPool.commonPool-worker-1	---come in
            ForkJoinPool.commonPool-worker-1	---come in
            30

           【以下情况都有可能（注：此为在JDK8下运行[已多次测试]）】
            ForkJoinPool.commonPool-worker-1	---come in
            ForkJoinPool.commonPool-worker-1	---come in
            main	---come in

            ForkJoinPool.commonPool-worker-1	---come in
            ForkJoinPool.commonPool-worker-2	---come in
            main	---come in
            30
        * */

    }

    /**
     * 对计算速度进行选用 【场景：多个客户端 pk，谁快谁赢】
     */
    @Test
    public void test4() {
        System.out.println(CompletableFuture.supplyAsync(() -> {
            //暂停几秒钟线程
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
        }).applyToEither(CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 2;
        }), r -> {
            return r;
        }).join());

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(3); } catch (InterruptedException e) { e.printStackTrace(); }

        /*
        * 执行结果：1
        *
        * */
    }

    /**
     * 对计算结果 进行消费
     */
    @Test
    public void test3() {
        CompletableFuture.supplyAsync(() -> {
            return 1;
        }).thenApply(f -> {
            return f+2;
        }).thenApply(f -> {
            return f+3;
        }).thenAccept(r -> System.out.println(r));  //执行结果：6


        System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenRun(() -> {}).join()); //执行结果：null


        System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenAccept(resultA -> {}).join()); //执行结果：null


        // ** 个解：下面lambda表达式中的 return关键字 可省略
        System.out.println(CompletableFuture.supplyAsync(() -> "resultA").thenApply(resultA -> resultA + " resultB").join()); //执行结果：resultA resultB
    }


    /**
     * 对计算结果 进行处理
     */
    @Test
    public void test2() {
        ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor
                        (1, 20, 1L, TimeUnit.SECONDS,
                                new LinkedBlockingQueue<>(50), Executors.defaultThreadFactory(),
                                                                        new ThreadPoolExecutor.AbortPolicy());

        System.out.println(CompletableFuture.supplyAsync(() -> {
            return 1;
        }).handle((f,e) -> {
            System.out.println("-----1");
            return f + 2;
        }).handle((f,e) -> {
            System.out.println("-----2");
            return f + 3;
        }).handle((f,e) -> {
            System.out.println("-----3");
            return f + 4;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("----result: " + v);
            }
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        }).join());


        threadPoolExecutor.shutdown();

        /*
        * 执行结果：
        *   -----1
            -----2
            -----3
            ----result: 10
            10
        * */
    }



    /**
     * 获得结果 和 触发计算
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test
    public void test1() throws InterruptedException, ExecutionException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            //暂停几秒钟线程
            try { TimeUnit.SECONDS.sleep(2); } catch (InterruptedException e) { e.printStackTrace(); }
            return 1;
        },threadPoolExecutor);

        //System.out.println(future.get());【情况一】
        //System.out.println(future.get(2L,TimeUnit.SECONDS));【情况二】
        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        //这个getNow(xxx) 为 若还没完成计算，给的一个 默认值
        //System.out.println(future.getNow(9999));【情况三】


        //这个 boolean complete(xxx) 为 打断成功/打断失败【注：这个方法的返回值是boolean】
        System.out.println(future.complete(-44)+"\t"+future.get()); //执行结果： true	 -44 【情况四】


        threadPoolExecutor.shutdown();
    }
}
