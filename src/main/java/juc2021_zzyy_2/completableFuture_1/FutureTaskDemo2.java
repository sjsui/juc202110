package juc2021_zzyy_2.completableFuture_1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeoutException;

/**
 * @author Suisijia
 * @create 2021-10-15 2:43
 */


// 此类为 针对futureTask的get()阻塞 的 两种解决方式。
// 但都不太好，最好还是 改用CompletableFuture

public class FutureTaskDemo2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println("----------班长去买水----------");
            Thread.sleep(5000);
            return "拿到一瓶水";
        });

        new Thread(futureTask,"AA").start();



        //解决方式一：
        //System.out.println(futureTask.get(3L, TimeUnit.SECONDS));//过时不候

        //解决方式二：不要阻塞，尽量用轮询替代
        while(true) {
            Thread.sleep(1000);
            if(futureTask.isDone()) {
                System.out.println("----result: " + futureTask.get());
                break;
            } else {
                // ** 个人理解：此时可正常 做主线程的事【此处改了老师原版输出，理解方式不一定对】
                // ？？【暂没深究】20240429个人猜测理解：貌似之前上面的理解不对，可能就是普通的自旋，[也不用sleep]只是为了让cpu[也即线程]不停
                System.out.println("------------老师继续讲课1------------");
            }
        }

        System.out.println("------------老师继续讲课2------------");

    }
}
