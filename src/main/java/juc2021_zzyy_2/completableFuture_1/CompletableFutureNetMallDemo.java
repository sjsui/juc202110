package juc2021_zzyy_2.completableFuture_1;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @auther zzyy
 * @create 2021-03-08 15:28
 *
 *
 * （重要）
 *
 * 案例说明：电商比价需求
 * 1 同一款产品，同时搜索出 同款产品 在各大电商的售价;
 * 2 同一款产品，同时搜索出 本产品 在某一个电商平台下，各个入驻门店 的售价
 *
 * 出来结果 希望 是 同款产品 在不同地方的 价格清单列表，返回一个List<String>
 * 《mysql》 in jd price is 88.05
 * 《mysql》 in pdd price is 86.11
 * 《mysql》 in taobao price is 90.43
 *
 * 3 要求深刻理解
 *   3.1 函数式编程
 *   3.2 链式编程
 *   3.3 Stream流式计算
 */
public class CompletableFutureNetMallDemo {
    static List<NetMall> list = Arrays.asList(
            new NetMall("jd"),
            new NetMall("pdd"),
            new NetMall("taobao"),
            new NetMall("dangdangwang"),
            new NetMall("tmall")
    );

    //同步 ，step by step

    /**
     * List<NetMall>  ---->   List<String>
     * @param list
     * @param productName
     * @return
     */
    public static List<String> getPriceByStep(List<NetMall> list,String productName) {
        return list
                .stream().
                map(netMall -> String.format(productName + " in %s price is %.2f", netMall.getMallName(), netMall.calcPrice(productName)))
                .collect(Collectors.toList());
    }

    //异步 ，多箭齐发

    /**
     * List<NetMall>  ---->List<CompletableFuture<String>> --->   List<String>
     * @param list
     * @param productName
     * @return
     */
    public static List<String> getPriceByASync(List<NetMall> list,String productName) {
        return list
                .stream()
                .map(netMall -> CompletableFuture.supplyAsync(() -> String.format(productName + " in %s price is %.2f", netMall.getMallName(), netMall.calcPrice(productName))))
                .collect(Collectors.toList())
                // * 纯个解：至此，返回的是一个List<CompletableFuture<String>> completableList 的 对象
                .stream()
                .map(CompletableFuture::join) //等同于 map(s -> s.join())
                .collect(Collectors.toList());

        /*
        *  **个人解释：
        * 1、
        * 下面这段 会返回一个 CompletableFuture<String> cf对象【参考CompletableFutureDemo.java 的 future3】。
        *
        * CompletableFuture.supplyAsync(() -> String.format(productName + " in %s price is %.2f", 参数1, 参数2))
        *
        * 2、
        * .map(CompletableFuture::join) //等同于 map(s -> s.join())。
        * 这行即为 把<CompletableFuture<String>> 映射为 <String>
        *
        * 3、
        * .collect(Collectors.toList())：会将结果收集为一个list
        *
        *
        *  ** 个解：综合来看，即为 给每个netMall，都开了一个线程 来处理。且优于FutureTask的写法、不会阻塞
        *
        * */
    }

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        List<String> list1 = getPriceByStep(list, "mysql");
        for (String element : list1) {
            System.out.println(element);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("----costTime: "+(endTime - startTime) +" 毫秒");


        System.out.println();


        long startTime2 = System.currentTimeMillis();
        List<String> list2 = getPriceByASync(list, "mysql");
        for (String element : list2) {
            System.out.println(element);
        }
        long endTime2 = System.currentTimeMillis();
        System.out.println("----costTime: "+(endTime2 - startTime2) +" 毫秒");

    }
}

class NetMall {
    @Getter
    private String mallName;

    public NetMall(String mallName)
    {
        this.mallName = mallName;
    }

    public double calcPrice(String productName) {
        //假设 检索需要1秒钟
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        // * 模拟一个随机价格（高并发下，最好用 ThreadLocalRandom 做随机）
        double v1 = ThreadLocalRandom.current().nextDouble() * 2;
        char c = productName.charAt(0);
        double v2 = v1 + c;
        return v2;
    }
}

/*
* 执行结果示例：
    mysql in jd price is 110.10
    mysql in pdd price is 109.72
    mysql in taobao price is 110.57
    mysql in dangdangwang price is 109.77
    mysql in tmall price is 110.75
    ----costTime: 5293 毫秒

    mysql in jd price is 110.29
    mysql in pdd price is 110.83
    mysql in taobao price is 109.60
    mysql in dangdangwang price is 109.46
    mysql in tmall price is 109.71
    ----costTime: 1026 毫秒
*
* */
