package juc2021_zzyy_2.completableFuture_1;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Suisijia
 * @create 2021-10-15 0:41
 */
public class FutureTaskDemo1 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println("----------班长去买水----------");
            Thread.sleep(5000);
            return "拿到一瓶水";
        });

        new Thread(futureTask,"AA").start();



        System.out.println("------------老师继续讲课------------");


        // ** FutureTask的 get() 会阻塞，因此要放到最后写 或 加一个超时时间
        System.out.println(futureTask.get());

    }

}
