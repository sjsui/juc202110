package juc2021_zzyy_2.completableFuture_1;

/**
 * @author Suisijia
 * @create 2021-10-15 0:16
 */

// * 说明了 所有用户线程都结束了，则jvm即会退出

public class DaemonDemo {
    public static void main(String[] args) {

        Thread aa = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " come in: \t" +
                    (Thread.currentThread().isDaemon() ? "守护线程" : "用户线程"));
            //为保证此线程不会结束
            while (true) {

            }
        },"aa");

        //要在线程开启之前，设置守护线程
        aa.setDaemon(true);
        aa.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("main thread is over");
    }
}

/*
* 执行结果：
*   aa come in: 	守护线程
    main thread is over
    [灯会灭，说明程序结束]

  若把 aa.setDaemon(true); 注释掉 的 执行结果：
    aa come in: 	用户线程
    main thread is over
    [灯不灭，说明程序没结束，线程aa还活着]

* */