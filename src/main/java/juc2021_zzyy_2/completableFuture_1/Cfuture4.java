package juc2021_zzyy_2.completableFuture_1;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * @author Suisijia
 * @create 2021-10-15 14:11
 */


//个解：CompletableFuture 的正确用法
// （结束后主动通知[个解：还是在CompletableFuture对应的线程中]，而不是[在main中]用get()）【连写的方式】
public class Cfuture4 {

    public static void main(String[] args) throws Exception {
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            int result = ThreadLocalRandom.current().nextInt(10);

            //暂停几秒钟线程
            //（个解：这块停了一下，因此，打印结果中会先打印"main 【main thread is over】"）
            //
            try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

            System.out.println(Thread.currentThread().getName() + "【-----计算结束耗时1秒钟，result:" + result + "】");
            if (result > 6) {
                int age = 10 / 0;
            }
            return result;
        }).whenComplete((v, e) -> {
            if (e == null) {
                // 测试后的个解：这句话是在 ForkJoinPool.commonPool-worker-1线程中打印的
                System.out.println(Thread.currentThread().getName() + "【-----result:" + v + "】");
            }
        }).exceptionally(e -> {
            // 测试后的个解：这句话是在 ForkJoinPool.commonPool-worker-1线程中打印的
            System.out.println(Thread.currentThread().getName() + "【-----exception:" + e.getCause() + "\t" + e.getMessage() + "】");
            return -44;
        });


        System.out.println(Thread.currentThread().getName() + " 【main thread is over】");

        // ** 主线程不要立刻结束，否则CompletableFuture 默认使用的线程池 会立刻关闭，导致无法接收结果。因此此处暂停3秒钟线程
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

/*
* 执行结果示例1：
    main 【main thread is over】
    ForkJoinPool.commonPool-worker-1【-----计算结束耗时1秒钟，result:4】
    ForkJoinPool.commonPool-worker-1【-----result:4】
*
* 执行结果示例2：
    main 【main thread is over】
    ForkJoinPool.commonPool-worker-1【-----计算结束耗时1秒钟，result:8】
    ForkJoinPool.commonPool-worker-1【-----exception:java.lang.ArithmeticException: / by zero	java.lang.ArithmeticException: / by zero】
*
* */



