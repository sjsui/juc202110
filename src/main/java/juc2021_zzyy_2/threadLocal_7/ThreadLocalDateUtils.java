package juc2021_zzyy_2.threadLocal_7;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @auther zzyy
 * @create 2021-03-23 15:46
 */
public class ThreadLocalDateUtils {
    //原版
//    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//    public static Date parse(String stringDate) throws ParseException {
//        return sdf.parse(stringDate);
//    }

    //解决一： 加锁
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static synchronized Date parse1(String stringDate) throws ParseException {
        return sdf.parse(stringDate);
    }


    //** 解决二：ThreadLocal可确保每个线程 都可得到 各自单独的一个SimpleDateFormat的对象，也就不存在竞争问题了。
    public static final ThreadLocal<SimpleDateFormat> sdfThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    public static final Date parseByThreadLocal(String stringDate) throws ParseException {
        return sdfThreadLocal.get().parse(stringDate);
    }


    //解决三【个人理解：这也是一种解决方式】： DateTimeFormatter 代替 SimpleDateFormat

    /*说明：如果是 JDK8，可使用 Instant 代替 Date，LocalDateTime 代替 Calendar，
    DateTimeFormatter 代替 SimpleDateFormat，
    官方解释：simple beautiful strong immutable thread-safe。*/

    public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    //个人理解：LocalDateTime -> String
    public static String format(LocalDateTime localDateTime) {
        return DATE_TIME_FORMAT.format(localDateTime);
    }

    //个人理解：String -> LocalDateTime
    public static LocalDateTime parse2(String dateString) {
        return LocalDateTime.parse(dateString,DATE_TIME_FORMAT);
    }



    public static void main(String[] args) throws ParseException {
        for (int i = 1; i <=5; i++) {
            new Thread(() -> {
                try {
                    //对应解决一：
                    //System.out.println(ThreadLocalDateUtils.parse1("2011-11-11 11:11:11"));

                    //对应解决二：
                    System.out.println(ThreadLocalDateUtils.parseByThreadLocal("2011-11-11 11:11:11"));

                    /*
                    其他方式：每次new一个。笨办法，不用它。
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    System.out.println(sdf.parse("2011-11-11 11:11:11"));
                    sdf = null;*/

                } catch (ParseException e) {
                    e.printStackTrace();
                } finally {
                    //别忘记threadLocal要remove()
                    //remove();
                }
            },String.valueOf(i)).start();
        }
    }
}
