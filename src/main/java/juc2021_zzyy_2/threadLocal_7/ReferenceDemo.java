package juc2021_zzyy_2.threadLocal_7;

import java.lang.ref.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

class MyObject {
    //一般这个方法工作中不用，此处为了讲解gc，才这么写
    @Override
    protected void finalize() throws Throwable {
        System.out.println("------------- gc ,finalize() invoked");
    }
}

/**
 * @auther zzyy
 * @create 2021-03-24 10:31
 */
public class ReferenceDemo {
    public static void main(String[] args) {
        //strongReference();
        //softReference();
        //weakReference();
        //phantomReference();
    }

    public static void strongReference() {
        MyObject myObject = new MyObject();//默认，强引用,死了都不放手
        System.out.println("gc before: "+myObject);

        //（除非手动置为null，才会被回收。平时遇到的 都是这种）
        myObject = null;


        System.gc();//手动挡的方式开启Gc回收。

        //给它1s 等它实际回收
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println("gc after: "+myObject);

        /*
        * 打印结果：
        *   gc before: juc2021_dachang.threadLocal.MyObject@14ae5a5
            gc after: juc2021_dachang.threadLocal.MyObject@14ae5a5

          打开 myObject = null; 之后的打印结果：
            gc before: juc2021_dachang.threadLocal.MyObject@14ae5a5
            ------------- gc ,finalize() invoked
            gc after: null
        *
        * */
    }


    public static void softReference() {
        SoftReference<MyObject> softReference = new SoftReference<>(new MyObject());//软引用

        //1 内存够用时的场景
/*        System.out.println("gc before内存够用: "+softReference);

        System.gc();//手动挡的方式开启Gc回收。
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println("gc after内存够用: "+softReference);*/



        //2 内存不够用时的场景（设置参数-Xms10m -Xmx10m【别忘了启动前先设置好！】）
        System.out.println("gc before: "+softReference);

        System.gc();//手动挡的方式 开启Gc回收
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println("gc before够用: "+softReference);

        try {
            //创建一个9m的对象。
            byte[] bytes = new byte[9 * 1024 * 1024];

            //此时无需手动gc，系统就会去gc

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("gc after内存不够: "+softReference.get());
        }

        /*
        * 内存够用时 打印结果：
        *   gc before内存够用: java.lang.ref.SoftReference@14ae5a5
            gc after内存够用: java.lang.ref.SoftReference@14ae5a5 【说明内存够时 不会回收】
        *
        * 内存不够时 打印结果：
            gc before: java.lang.ref.SoftReference@14ae5a5
            gc before够用: java.lang.ref.SoftReference@14ae5a5 【说明内存够时 不会回收】
            gc after内存不够: null  【说明内存不够时 才会回收】
            ------------- gc ,finalize() invoked
            Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
            at ...
            at ...
        *
        * */
    }


    public static void weakReference() {
        WeakReference<MyObject> weakReference = new WeakReference(new MyObject());
        System.out.println("gc before: "+weakReference.get());

        System.gc();//手动挡的方式开启Gc回收。
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println("gc after: "+weakReference.get());

        /*
        * 打印结果：
        *   gc before: juc2021_dachang.threadLocal.MyObject@14ae5a5
            ------------- gc ,finalize() invoked
            gc after: null
        *
        * */
    }



    //（了解。基本不用）（设置参数-Xms15m -Xmx15m【别忘了启动前先设置好！】）
    public static void phantomReference() {
        ReferenceQueue<MyObject> referenceQueue = new ReferenceQueue<>();
        PhantomReference<MyObject> myObjectPhantomReference = new PhantomReference<>(new MyObject(), referenceQueue);
        System.out.println(myObjectPhantomReference.get());

        List<byte[]> list = new ArrayList<>();

        new Thread(() -> {
            while (true) {
                //每0.5s 往list中 加 1m
                list.add(new byte[1 * 1024 * 1024]);
                try { TimeUnit.MILLISECONDS.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); }
                System.out.println(myObjectPhantomReference.get());
            }
        },"t1").start();

        new Thread(() -> {
            while(true) {
                Reference<? extends MyObject> poll = referenceQueue.poll();
                if (poll != null) {
                    System.out.println("------有虚对象进入了队列");
                }
            }
        },"t2").start();

        //暂停几秒钟线程（为了看结果）
        try { TimeUnit.SECONDS.sleep(5); } catch (InterruptedException e) { e.printStackTrace(); }

        /*
        * 打印结果：
            null
            null
            null
            null
            null
            null
            null
            null
            null
            null
            ------------- gc ,finalize() invoked
            null
            null
            null
            ------有虚对象进入了队列
            null
            Exception in thread "t1" java.lang.OutOfMemoryError: Java heap space
        *
        * */

    }

}
