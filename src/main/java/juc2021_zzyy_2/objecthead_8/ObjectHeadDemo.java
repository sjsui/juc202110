package juc2021_zzyy_2.objecthead_8;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * @auther zzyy
 * @create 2021-03-26 15:27
 */
public class ObjectHeadDemo {
    public static void main(String[] args) {

        //JVM的 细节 详细情况（此VM类 也是JOL包中的）
        System.out.println(VM.current().details());

        /*
        * 打印结果：
        *   # Running 64-bit HotSpot VM.
            # Using compressed oop with 3-bit shift.
            # Using compressed klass with 3-bit shift.
            # Objects are 8 bytes aligned.
            # Field sizes by type: 4, 1, 1, 2, 2, 4, 4, 8, 8 [bytes]
            # Array element sizes: 4, 1, 1, 2, 2, 4, 4, 8, 8 [bytes]
        *
        * */
        //所有对象 分配的字节 都是8的整数倍。（此VM类 也是JOL包中的）
        System.out.println(VM.current().objectAlignment()); //8


        //引入了JOL，直接使用
        Object object = new Object();
        System.out.println(ClassLayout.parseInstance(object).toPrintable());

        /*
        * 打印结果：
        *   java.lang.Object object internals:
             OFFSET  SIZE   TYPE DESCRIPTION                               VALUE
                  0     4        (object header)                           05 00 00 00 (00000101 00000000 00000000 00000000) (5)
                  4     4        (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
                  8     4        (object header)                           e5 01 00 f8 (11100101 00000001 00000000 11111000) (-134217243)
                 12     4        (loss due to the next object alignment)
            Instance size: 16 bytes
            Space losses: 0 bytes internal + 4 bytes external = 4 bytes total
        *
        * */


        System.out.println(ClassLayout.parseInstance(new MyObject()).toPrintable());

        /*
        * 打印结果：
            juc2021_dachang.objecthead.MyObject object internals:
             OFFSET  SIZE      TYPE DESCRIPTION                               VALUE
                  0     4           (object header)                           05 00 00 00 (00000101 00000000 00000000 00000000) (5)
                  4     4           (object header)                           00 00 00 00 (00000000 00000000 00000000 00000000) (0)
                  8     4           (object header)                           d2 23 01 f8 (11010010 00100011 00000001 11111000) (-134143022)
                 12     4       int MyObject.i                                25
                 16     8      long MyObject.l                                66
                 24     1   boolean MyObject.flag                             false
                 25     7           (loss due to the next object alignment)
            Instance size: 32 bytes
            Space losses: 0 bytes internal + 7 bytes external = 7 bytes total
        *
        * */



        //java5之前 只有重量级锁
        new Thread(() -> {
            synchronized (object){
                System.out.println("----hello juc");    //----hello juc
            }
        },"t1").start();

    }
}


class MyObject {
    int i = 25;
    boolean flag = false;
    long l = 66;
}